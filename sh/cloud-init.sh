#!/usr/bin/env bash
USER=lewisasellers
GIT=https://lewisasellers@bitbucket.org/lewisasellers/intrafoundation.git
NAME="Lewis A. Sellers"
EMAIL=lasellers@gmail.com
WEBPARENT=/usr/share/nginx
WEBROOT=/usr/share/nginx/html

yum update -y
yum install -y php70 php70-fpm
yum install -y php70-devel
yum install -y php70-mbstring
yum install -y php70-pdo
yum install -y php70-gd
yum install -y php70-imap
yum install -y php70-opcache
yum install -y php70-pecl-apcu
yum install -y nginx
yum install -y git-core
#yum install -y mysql57-server
#yum install -y php70-mysql
#yum install -y php70-mysqlnd

chkconfig nginx on
chkconfig php-fpm on
#chkconfig mysqld on

#groupadd www
#usermod -a -G www ec2-user
#usermod -a -G www root

#usermod -a -G nginx ec2-user

cd $WEBPARENT

#chmod 2775 -R $WEBPARENT
find $WEBPARENT -type d -exec chmod 2775 {} \;
find $WEBPARENT -type f -exec chmod 0664 {} \;
chown -R ec2-user:nginx $WEBPARENT

git config --global user.name "$NAME"
git config --global user.email "$EMAIL"

mkdir $WEBPARENT/tmp
git clone $GIT tmp
mv $WEBPARENT/tmp/* $WEBROOT/
mv $WEBPARENT/tmp/.[!.]* $WEBROOT/

\cp $WEBROOT/sh/nginx.conf /etc/nginx/nginx.conf -f
\cp $WEBROOT/sh/www.conf /etc/php-fpm.d/www.conf -f

cd $WEBROOT

echo "" > $WEBROOT/storage/logs/laravel.log
truncate -s 0 $WEBROOT/storage/logs/laravel.log
chmod 666 $WEBROOT/storage/* -R

curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer -f
ln -s /usr/local/bin/composer /usr/bin/composer

composer self-update
composer update
composer diagnose
#composer self-update --update-keys
composer dump-autoload

php artisan clear-compiled
php artisan cache:clear
php artisan view:clear
php artisan route:clear
php artisan route:cache
php artisan route:list
php artisan optimize

composer -V
php artisan --version

#echo "<?php phpinfo(); ?>" > $WEBROOT/public/phpinfo.php

find $WEBPARENT -type d -exec chmod 2775 {} \;
find $WEBPARENT -type f -exec chmod 0664 {} \;
chown -R ec2-user:nginx $WEBPARENT *

service nginx restart
service php-fpm restart
#service mysqld restart

more storage/logs/laravel.log
more /var/log/nginx/error.log
