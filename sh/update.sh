#!/usr/bin/env bash
composer self-update
composer update
composer diagnose
php artisan cache:clear
php artisan view:clear
php artisan dump-autoload
php artisan clear-compiled
php artisan routes:clear
php artisan optimize
php artisan route:cache
php artisan route:list

composer -V
php artisan --version
