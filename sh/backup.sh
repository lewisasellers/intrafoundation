#!/usr/bin/env bash
tar --exclude="storage/cache/*"  --exclude="storage/logs/*"  --exclude="storage/sessions/*"  --exclude=".git/*" -zcvf ~/intrafoundation.tar.gz /var/www/intrafoundation
/usr/bin/mysqldump -u root intrafoundation > ~/intrafoundation.sql
#du -max-depth=1 -h > app/du.txt
