$(document).ready(function(){
      console.log("api ready");
    /*
    GET
api/{member_token}

      @author: Lewis A. Sellers <lasellers@gmail.com>
      @date: 12/2013
      */
      $(".test-get-api").click(function(e){
        e.preventDefault();

        var member_token=$("#member_token_1").val();

        $("#get-api").fadeOut('fast');

        $.ajax({
            type: "GET",
            url: "/api/"+blank_string(member_token)
        }).done(function( msg ) {
            $("#get-api").fadeIn('fast');
            var msg2=JSON.stringify(msg, null, 4)
            $("#get-api").text(msg2);
        });
      });

      /*
      GET
api/codes/{member_token}

      @author: Lewis A. Sellers <lasellers@gmail.com>
      @date: 12/2013
      */
      $(".test-get-codes").click(function(e){
            e.preventDefault();

            var member_token=$("#member_token_2").val();

            $("#get-codes").fadeOut('fast');

            $.ajax({
                  type: "GET",
                  url: "/api/codes/"+blank_string(member_token)
            }).done(function( msg ) {
                  $("#get-codes").fadeIn('fast');
                  var msg2=JSON.stringify(msg, null, 4)
                  $("#get-codes").text(msg2);
            });
      });
      /*
      GET
api/code/{member_token}/{id}

      @author: Lewis A. Sellers <lasellers@gmail.com>
      @date: 12/2013
      */
      $(".test-get-code").click(function(e){
            e.preventDefault();

            var member_token=$("#member_token_3").val();
            var id=$("#id_3").val();
                        var v=$("#v_3").val();

            $("#get-code").fadeOut('fast');

            $.ajax({
                  type: "GET",
                  url: "/api/code/"+blank_string(member_token)+"/"+blank_number(id)+"/"+blank_number(v)
            }).done(function( msg ) {
                  $("#get-code").fadeIn('fast');
                  var msg2=JSON.stringify(msg, null, 4)
                  $("#get-code").text(msg2);
            });
      });
      /*
      GET
api/code/comments/{member_token}/{id}

      @author: Lewis A. Sellers <lasellers@gmail.com>
      @date: 12/2013
      */
      $(".test-get-code-comments").click(function(e){
            e.preventDefault();

            var member_token=$("#member_token_4").val();
            var id=$("#id_4").val();
     var v=$("#v_4").val();

            $("#get-code-comments").fadeOut('fast');

            $.ajax({
                  type: "GET",
                  url: "/api/code/comments/"+blank_string(member_token)+"/"+blank_number(id)+"/"+blank_number(v)
            }).done(function( msg ) {
                  $("#get-code-comments").fadeIn('fast');
                  var msg2=JSON.stringify(msg, null, 4)
                  $("#get-code-comments").text(msg2);
            });
      });
    

  });

function blank_string(s)
{
    return s==''?'NULL':s;
}
function blank_number(s)
{
    return s==''?'0':s;
}