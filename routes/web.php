<?php

/*
Route::controllers([
  'auth' => 'Auth\AuthController',
  'password' => 'Auth\PasswordController',
]);
*/

Route::get('about-us', 'PublicController@about_us');
Route::get('contact-us', 'PublicController@contact_us');
Route::post('contact-us', 'PublicController@contact_us_post');

Route::group(array('prefix' => 'news'), function () {
    Route::get('/{id}', 'NewsController@view');
    Route::get('/', 'NewsController@index');
});

Route::group(array('prefix' => 'software'), function () {
    Route::get('/category/{category}', 'SoftwareController@category');
    Route::get('/manual/{code}', 'SoftwareController@product_manual');
    Route::get('/link', 'SoftwareController@link');
    Route::get('/download/{code}', 'SoftwareController@download');
    Route::get('/{code}', 'SoftwareController@product');
    Route::get('/', 'SoftwareController@index');
});

Route::group(array('prefix' => 'comments'), function () {
    Route::get('/view/{id}', 'CommentsController@view');
    Route::get('/view/thread/{thread_id}', 'CommentsController@view_thread');
    Route::get('/product-code/{product_code}', 'CommentsController@product_code');
    Route::get('/add', 'CommentsController@comments_add');
    Route::post('/add', 'CommentsController@comments_add_post');
    Route::get('/reply', 'CommentsController@comments_reply');
    Route::post('/reply', 'CommentsController@comments_reply_post');
    Route::get('/edit', 'CommentsController@comments_edit');
    Route::post('/edit', 'CommentsController@comments_edit_post');
    Route::get('/delete', 'CommentsController@comments_delete');
    Route::get('/', 'CommentsController@index');
});

Route::group([], function () {
// Authentication Routes...
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
});

//Route::get('members', 'PublicController@members');
Route::get('sitemap.xml', 'PublicController@sitemap_xml');

/*
 * Route::group(array('prefix' => 'product'), function() {
            Route::get('/{id?}', array('as' => 'product.index', 'uses' => 'ProductController@index'));
        });

 */
/*
Route::post('home', function () {
    return redirect('/')->with('status', 'Login');
});
Route::get('home', function () {
    return redirect('/');
});
*/
Route::get('/home', 'PublicController@home');
Route::get('/', 'PublicController@index');
/*Route::get('/', function () {
    return view('welcome');
});*/

//Route::get('admin/login', 'AdminController@login');
//Route::post('admin/login', array('uses' => 'AdminController@login_post'));
//Route::get('admin/logout', 'AdminController@logout');

Route::get('/test/web', function () {
    return view('test')
        ->with('meta_title', 'web')
        ->with('categories', []);
});

Route::get('test/web2', function () {
    return view('test')
        ->with('meta_title', 'web')
        ->with('categories', []);
});


Event::listen('404', function () {
    return Response::error('404');
});

Event::listen('500', function () {
    return Response::error('500');
});
Auth::routes();

//Route::get('/home', 'HomeController@index');
