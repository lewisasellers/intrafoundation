<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        Eloquent::unguard();
        $this->call('SchedulesTableSeeder');
    }
}
class SchedulesTableSeeder extends Seeder
{
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        
        //Config::get('database.connections.'.Config::get('database.default').'.database')
        $dbconfig=[
        'host' => env('DB_HOST'),
        'database' => env('DB_DATABASE'),
        'username' => env('DB_USERNAME'),
        'password' => env('DB_PASSWORD'),
        'import'=> dirname(__FILE__).'/intrafoundation.sql'
        ];
        
        /*
        	$host=Config::get('database.connections.mysql.host');
		$username=Config::get('database.connections.mysql.username');
		$database=Config::get('database.connections.mysql.database');
		$password=Config::get('database.connections.mysql.password');
        /*/
        
        //DONT EDIT BELOW THIS LINE
        //Export the database and output the status to the page
//        $command='mysql -h ' .$dbconfig['host'] .' -u ' .$dbconfig['username'] .' -p ' .$dbconfig['password'] .' ' .$dbconfig['database'] .' < ' .$dbconfig['import'];
        $command='mysql ' .$dbconfig['database'] .' < ' .$dbconfig['import'];
        echo "command=$command\n";
        exec($command,$output=[],$worked);
        switch($worked){
            case 0:
                echo 'Import file <b>' .$dbconfig['import'] .'</b> successfully imported to database <b>' .$dbconfig['database'] .'</b>';
                break;
            case 1:
                echo 'There was an error during import. Please make sure the import file is saved in the same folder as this script and check your values:<br/><br/><table><tr><td>MySQL Database Name:</td><td><b>' .$mysqlDatabaseName .'</b></td></tr><tr><td>MySQL User Name:</td><td><b>' .$mysqlUserName .'</b></td></tr><tr><td>MySQL Password:</td><td><b>NOTSHOWN</b></td></tr><tr><td>MySQL Host Name:</td><td><b>' .$mysqlHostName .'</b></td></tr><tr><td>MySQL Import Filename:</td><td><b>' .$mysqlImportFilename .'</b></td></tr></table>';
                break;
    }
    
    /*
    
    $lorem="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed auctor sapien et lacus molestie, vel imperdiet tortor suscipit. Integer egestas euismod nibh a auctor. Nullam posuere nunc id turpis semper, nec feugiat nunc rhoncus. Mauris eget risus et arcu scelerisque ultricies. Vivamus bibendum, tortor sed suscipit dignissim, diam eros iaculis massa, in scelerisque eros libero ac nisl. Nunc in neque neque. Phasellus condimentum quis massa quis luctus. In magna ipsum, sagittis ut velit nec, adipiscing dapibus nunc. Phasellus a elementum est, mollis venenatis diam.
    ";
    
    DB::statement("TRUNCATE TABLE member");
    DB::statement("ALTER TABLE member AUTO_INCREMENT =1");
    
    $member=new Member;
    $member->member_id=1;
    $member->email="webmaster@communalperfection.com";
    $member->public_name="Lewis A. Sellers";
    $member->nick_name="Lewis";
    $member->plain_password="kindzadza";
    $member->password=Hash::make($member->plain_password);
    $member->save();
    echo "member #{$member->member_id}\n";
    
    $member=new Member;
    $member->member_id=2;
    $member->email="lasellers@gmail.com";
    $member->public_name="Lewis A. Sellers";
    $member->nick_name="Lewis";
    $member->plain_password="kindzadza";
    $member->password=Hash::make($member->plain_password);
    $member->save();
    echo "member #{$member->member_id}\n";
    
    $member=new Member;
    $member->member_id=3;
    $member->email="tommyraven@communalperfection.com";
    $member->public_name="Lewis A. Sellers";
    $member->nick_name="Lewis";
    $member->plain_password="kindzadza";
    $member->password=Hash::make($member->plain_password);
    $member->save();
    echo "member #{$member->member_id}\n";
    
    
    $member=new Member;
    $member->member_id=4;
    $member->email="cjrsellers@gmail.com";
    $member->public_name="Cynthia J. Sellers";
    $member->nick_name="Cynthia";
    $member->plain_password="kindzadza";
    $member->password=Hash::make($member->plain_password);
    $member->save();
    
    echo "member #{$member->member_id}\n";
    
    for($m=1;$m<20;$m++)
    {
    echo "friends... #{$m}\n";
    for($n=1;$n<20;$n++)
    {
    $friend=new Friend;
    $friend->member_id=$m;
    $friend->their_member_id=$n;
    $friend->save();
    
    }
    }
    
    for($m=1;$m<20;$m++)
    {
    echo "coworkers... #{$m}\n";
    for($n=1;$n<20;$n++)
    {
    $coworker=new Coworker;
    $coworker->member_id=$m;
    $coworker->their_member_id=$n;
    $coworker->save();
    
    }
    }
    
    //
    DB::statement("TRUNCATE TABLE comment_header");
    DB::statement("ALTER TABLE comment_header AUTO_INCREMENT =1");
    
    $comment_header=new CommentHeader;
    $comment_header->description=$lorem;
    $comment_header->member_id=1;
    $comment_header->title='test';
    $comment_header->save();
    
    echo "comment_header #{$comment_header->comment_header_id}\n";
    
    //
    DB::statement("TRUNCATE TABLE comment");
    DB::statement("ALTER TABLE comment AUTO_INCREMENT =1");
    
    
    for($n=1;$n<5;$n++)
    {
    $comment=new Comment;
    $comment->comment=$lorem;
    $comment->member_id=1;
    $comment->comment_header_id=1;
    $comment->save();
    echo "comment #{$comment->comment_id}\n";
    }
    
    //
    DB::statement("TRUNCATE TABLE message");
    DB::statement("ALTER TABLE message AUTO_INCREMENT =1");
    
    for($n=1;$n<5;$n++)
    {
    $message=new Message;
    $message->message=$lorem;
    $message->from_member_id=2;
    $message->member_id=1;
    $message->save();
    echo "message #{$message->message_id}\n";
    }
    
    
    for($n=1;$n<5;$n++)
    {
    $message=new Message;
    $message->message=$lorem;
    $message->from_member_id=1;
    $message->member_id=2;
    $message->subject='blah '.rand();
    $message->save();
    echo "message #{$message->message_id}\n";
    }
    
    
    //
    //
    DB::statement("TRUNCATE TABLE `group`");
    DB::statement("ALTER TABLE `group` AUTO_INCREMENT =1");
    
    $group=new Group;
    $group->name="test group";
    $group->summary=$lorem;
    $group->member_id=1;
    $group->description=$lorem;
    $group->save();
    
    for($n=2;$n<10;$n++)
    {
    $group=new Group;
    $group->name="test group ".rand();
    $group->summary=$lorem;
    $group->member_id=rand(1,3);
    $group->description=rand().$lorem;
    $group->save();
    
    }
    
    //
    DB::statement("TRUNCATE TABLE community_member");
    DB::statement("ALTER TABLE community_member AUTO_INCREMENT =1");
    
    $community_member=new GroupMember;
    $community_member->member_id=1;
    $community_member->community_id=1;
    $community_member->save();
    
    
    $community_member=new GroupMember;
    $community_member->member_id=1;
    $community_member->community_id=2;
    $community_member->save();
    
    
    
    //
    DB::statement("TRUNCATE TABLE code_abstract");
    DB::statement("ALTER TABLE code_abstract AUTO_INCREMENT =1");
    DB::statement("TRUNCATE TABLE code_variant");
    DB::statement("ALTER TABLE code_variant AUTO_INCREMENT =1");
    
    $code_abstract=new CodeAbstract;
    $code_abstract->member_id=1;
    $code_abstract->abstract_name='PI';
    $code_abstract->description="The number π is a mathematical constant that is the ratio of a circle's circumference to its diameter and is approximately equal to 3.14159. It has been represented by the Greek letter π since the mid-18th century, though it is also sometimes romanized as pi (/paɪ/).
    Being an irrational number, π cannot be expressed exactly as a ratio of any two integers (fractions such as 22/7 are commonly used to approximate π, but no fraction can be its exact value). Consequently, its decimal representation never ends and never settles into a permanent repeating pattern. The digits appear to be randomly distributed, although no proof of this has yet been discovered. Also, π is a transcendental number – a number that is not the root of any nonzero polynomial having rational coefficients. The transcendence of π implies that it is impossible to solve the ancient challenge of squaring the circle with a compass and straight-edge.
    For thousands of years, mathematicians have attempted to extend their understanding of π, sometimes by computing its value to a high degree of accuracy. Before the 15th century, mathematicians such as Archimedes and Liu Hui used geometrical techniques, based on polygons, to estimate the value of π. Starting around the 15th century, new algorithms based on infinite series revolutionized the computation of π. In the 20th and 21st centuries, mathematicians and computer scientists discovered new approaches that – when combined with increasing computational power – extended the decimal representation of π to, as of late 2011, over 10 trillion (1013) digits.[1] Scientific applications generally require no more than 40 digits of π, so the primary motivation for these computations is the human desire to break records, but the extensive calculations involved have been used to test supercomputers and high-precision multiplication algorithms.
    Because its definition relates to the circle, π is found in many formulae in trigonometry and geometry, especially those concerning circles, ellipses, or spheres. It is also found in formulae from other branches of science, such as cosmology, number theory, statistics, fractals, thermodynamics, mechanics, and electromagnetism. The ubiquitous nature of π makes it one of the most widely known mathematical constants, both inside and outside the scientific community: Several books devoted to it have been published, the number is celebrated on Pi Day, and record-setting calculations of the digits of π often result in news headlines. Attempts to memorize the value of π with increasing precision have led to records of over 67,000 digits.";
    $code_abstract->save();
    
    $code_abstract=new CodeVariant;
    $code_abstract->member_id=1;
    $code_abstract->variant_name='PI';
    $code_abstract->code_language='C';
    $code_abstract->code='Nulla ac elit ac sem condimentum porttitor. Sed blandit libero sed mattis fringilla. Fusce in risus eros. Aliquam tempus id nisi et scelerisque. Donec sit amet arcu nulla. Etiam et iaculis ipsum. Phasellus orci nisi, laoreet sed volutpat eu, lacinia eget elit. Sed semper volutpat quam id commodo. Curabitur eget erat laoreet, commodo lectus id, scelerisque odio. Proin sed purus vulputate, pretium augue non, consectetur sapien. In in lectus blandit magna auctor aliquet ac cursus felis. Mauris at turpis ut orci iaculis accumsan quis vel elit. Sed auctor nulla ac elit fringilla blandit. Etiam vitae mattis velit. Vestibulum odio sem, tempus rutrum metus et, ornare auctor urna. Praesent sed erat ut libero fermentum lacinia.
    ';
    $code_abstract->save();
    
    
    
    for($n=1;$n<30;$n++)
    {
    
    $code_abstract=new CodeAbstract;
    $code_abstract->member_id=rand(1,4);
    $code_abstract->abstract_name='lorem '.rand();
    $code_abstract->plain_name='lorem '.rand();
    $code_abstract->description=$lorem;
    $code_abstract->save();
    }
    
    
    for($n=1;$n<200;$n++)
    {
    
    $code_variant=new CodeVariant;
    $code_variant->member_id=rand(1,4);
    $code_variant->code_id=rand(1,30);
    $code_variant->variant_name='lorem '.rand();
    $code_variant->plain_name='lorem '.rand();
    $code_variant->description=$lorem;
    $code_variant->save();
    }
    */
}
}