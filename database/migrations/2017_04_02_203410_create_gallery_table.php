<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('gallery')) Schema::drop('gallery');

        Schema::create('gallery',
            function($table)
            {
                $table->increments('gallery_id');
                $table->string('title',200)->nullable();
                $table->text('description')->nullable();

                $table->integer('views')->default(0);
                $table->string('product_code',64)->nullable()->default(NULL);
                $table->integer('arrangement')->default(0);
                $table->integer('photograph_count')->default(0);
                $table->enum('category', array('gallery','portfolio'))->default('gallery');
                $table->string('url',255)->nullable()->default(NULL);

                $table->timestamps();
            });
      }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery');
    }
}
