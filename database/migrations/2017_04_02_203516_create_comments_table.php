<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable('comment')) Schema::drop('comments');

        Schema::create('comments',
            function (Blueprint $table) {

                $table->increments('id');

                $table->integer('thread_id')->default(0);
                $table->integer('threads')->default(0);
                $table->integer('user_id')->default(0);
                $table->string('source', 12)->nullable()->default('Forums');
                $table->string('title', 132)->nullable();
                $table->text('message')->nullable();
                $table->string('author', 200)->nullable();
                $table->string('email', 131)->nullable();
                $table->string('url', 255)->nullable();
                $table->text('code')->nullable();
                $table->string('product_code', 64)->nullable();

                $table->timestamps();

                $table->index('created_at');
                $table->index('updated_at');
                $table->index('thread_id');
                //$table->index('member_id');
            });

        //  DB::statement("ALTER TABLE comments AUTO_INCREMENT =1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
    }
}
