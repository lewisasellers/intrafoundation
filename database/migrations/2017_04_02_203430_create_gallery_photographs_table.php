<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryPhotographsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('gallery_photographs')) Schema::drop('gallery_photographs');

        Schema::create('gallery_photographs',
            function ($table) {
                $table->increments('photograph_id');
                $table->integer('gallery_id')->default(0);

                $table->string('title', 132)->nullable();
                $table->text('description')->nullable();
                $table->string('filename', 255)->nullable();

                $table->integer('views')->default(0);
                $table->integer('arrangement')->default(0);

                $table->timestamps();

                $table->index('gallery_id');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery_photographs');
    }
}
