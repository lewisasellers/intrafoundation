<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('product_links')) Schema::drop('product_links');

        Schema::create('product_links',
            function($table)
            {
                $table->increments('link_id');

                $table->integer('owner_member_id')->nullable();
                $table->string('url',255)->nullable();
                $table->integer('views')->nullable();
                $table->string('category', 32)->nullable();
                $table->string('platform', 32)->nullable();
                $table->string('technology', 32)->nullable();
                $table->string('product_code', 32)->nullable();
                $table->string('tags', 240)->nullable();
                $table->timestamps();

                $table->index('created_at');
                $table->index('category');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_links');
    }
}
