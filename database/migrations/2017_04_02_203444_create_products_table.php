<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('products')) Schema::drop('products');

        Schema::create('products',
            function ($table) {
                $table->increments('product_id');

                $table->string('product_code', 32)->default('');
                $table->string('version_type', 1)->default(2);
                $table->string('version', 20)->default('0.0.0');
                $table->boolean('stage')->default(0);

                $table->string('title', 132)->nullable();

                $table->string('name', 132)->default('');
                $table->string('category', 20)->default('Developer');

                $table->text('copyright')->nullable();

                $table->text('blurb')->nullable();
                $table->text('description')->nullable();

                $table->integer('views')->default(0);
                $table->integer('downloads')->default(0);

                $table->boolean('hidden')->default(0);
                $table->boolean('obsolete')->default(0);
                $table->boolean('shelved')->default(0);
                $table->boolean('experimental')->default(0);
                $table->boolean('pdf')->default(0);
                $table->boolean('third_party')->default(0);

                $table->string('file', 255)->nullable()->default(NULL);

                $table->integer('gallery_id')->nullable()->default(NULL);

                $table->boolean('source_code')->default(0);
                $table->string('source_language', 20)->default('');
                $table->string('compiler', 32)->default('');
                $table->string('platform', 32)->default('');
                $table->string('technology', 32)->default('');
                $table->text('platform_specs')->nullable();
                $table->string('bits', 12)->nullable()->default(NULL);

              //  $table->json('info');
                
                $table->datetime('last_download')->nullable()->default(NULL);
                $table->datetime('build_date')->nullable()->default(NULL);
                $table->timestamps();

                $table->index('product_code');
                $table->index('category');
                $table->index('gallery_id');
                $table->index('platform');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}


