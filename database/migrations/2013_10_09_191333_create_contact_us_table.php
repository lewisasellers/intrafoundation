<?php
use Illuminate\Database\Migrations\Migration;

class CreateContactUsTable extends Migration
{
    /**
     * Make changes to the database.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('contact_us')) Schema::drop('contact_us');

        Schema::create('contact_us',
            function ($table) {

                $table->increments('id');
                $table->string('name', 132);
                $table->string('email', 131);
                $table->string('url', 255);
                $table->text('comment');
                $table->timestamps(); //created_at,updated_at

                $table->index('updated_at');
            });

        // DB::statement("ALTER TABLE contact_us AUTO_INCREMENT =1");

// fetch populates the schedules table
        //  $fetch=new FetchCommand();
        //  $fetch->call_command();
    }

    /**
     * Revert the changes to the database.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contact_us');
    }
}

