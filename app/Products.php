<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/*
  abstract code ORM
 */

class Products extends Model
{
    private static $categories = array(
        "all" => array('title' => "All", 'long_title' => "All"),
        //"ios"=>array('title'=>"iOS",'long_title'=>"iOS"),
        //"android"=>array('title'=>"Android",'long_title'=>"Android"),
        "coldfusion" => array('title' => "ColdFusion", 'long_title' => "ColdFusion"),
        //"net"=>array('title'=>".Net",'long_title'=>".Net"),
        "php" => array('title' => "PHP", 'long_title' => "PHP"),
        //"java"=>array('title'=>"Java",'long_title'=>"Java"),
        "gaming" => array('title' => "Gaming", 'long_title' => "Gaming"),
        "dos" => array('title' => "DOS", 'long_title' => "DOS"),
        "windows" => array('title' => "Windows", 'long_title' => "Windows"),
        //"macintosh"=>array('title'=>"Macintosh",'long_title'=>"Macintosh"),
        //"ti"=>array('title'=>"TI",'long_title'=>"TI"),
    );
    protected $table = 'products';
    public $timestamps = true;
    protected $primaryKey = 'product_id';

    public static function get_stages()
    {
        return array("", "Alpha", "Beta", "Gamma", "Omega");
    }

    public static function get_categories()
    {
        return self::to_object(self::$categories);
    }

    /**
     * @param $category_id
     * @return mixed
     */
    public static function get_category($category_id)
    {
        if (isset(self::$categories[$category_id]))
            $obj = self::$categories[$category_id];
        else $obj = array(
            'title' => '',
            'long_title' => ''
        );

        return self::to_object($obj);
    }

    /**
     * @param $version_type
     * @param $version
     * @return string
     */
    public static function printVersion($version_type, $version): string
    {
        if ($version_type == "V") return "V" . $version;
        else if ($version_type == "2") return " version " . $version;
        else return " version " . $version;
    }

    /**
     * @param $array
     * @return mixed
     */
    public static function to_object($array)
    {
        return json_decode(json_encode($array), FALSE);
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public static function getDownloads(int $limit = 0)
    {
        $downloads = DB::select("SELECT * FROM products
        WHERE hidden=0 AND downloads>0
        ORDER BY downloads DESC LIMIT {$limit};");
        return $downloads;
    }

    /**
     * @param array|null $products
     * @return array
     */
    private static function addImagePath(array $products = null)
    {
        $products = array_map(function ($product) {
            $imagePath = public_path() . "/content/product_images/{$product->product_id}.jpg";
            $product->imagePath = file_exists($imagePath) ? $imagePath : null;

            $product->imageUrl = file_exists($imagePath) ? "/content/product_images/{$product->product_id}.jpg" : null;

            $filePath = public_path() . '/content/products/' . $product->file;
            $product->filePath = file_exists($filePath) ? $filePath : null;
            return $product;
        }, $products);

        return $products;
    }


    /**
     * @param int $limit
     * @return array
     */
    public static function getList(int $limit = 0): array
    {
        $products = DB::select("SELECT *
 FROM products WHERE hidden=0 AND downloads>0
        ORDER BY build_date DESC LIMIT {$limit};");
        $products = self::addImagePath($products);
        return $products;
    }


    /**
     * @param string $categoryId
     * @param int $limit
     * @return array
     */
    public static function getByCategory(string $categoryId = 'all', int $limit = 0): array
    {
        if ($categoryId == 'all')
            $products = DB::select(
                "SELECT p.* FROM products p WHERE p.hidden=0 ORDER BY p.downloads DESC;");
        else
            $products = DB::select(
                "SELECT p.* FROM products p WHERE p.hidden=0 AND (p.category=?) ORDER BY p.downloads DESC;",
                [$categoryId, $categoryId]);
        $products = self::addImagePath($products);
        return $products;
    }

    /**
     * @param string $productCode
     * @param int $limit
     * @return array
     */
    public static function getByProductCode(string $productCode = 'all', int $limit = 1): array
    {
        $products = DB::select("SELECT *
 FROM products WHERE product_code=? LIMIT ?;",
            [$productCode, $limit]);
        $products = self::addImagePath($products);
        return $products;
    }

    /**
     * @param string|null $productId
     * @param int $limit
     * @return array
     */
    public static function getById(string $productId = null, int $limit = 1): array
    {
        if ($productId == null) return [];
        $products = DB::select("SELECT *
 FROM products WHERE product_id=? LIMIT ?;",
            [$productId, $limit]);
        $products = self::addImagePath($products);
        return $products;
    }

    /**
     * @param int $productId
     */
    public static function updateViewsByProductId(int $productId)
    {
        DB::update(
            "UPDATE products SET views=views+1 WHERE product_id=?", [$productId]
        );
    }

    /**
     * @param int $productCode
     */
    public static function updateViewsByProductCode(int $productCode)
    {
        DB::update(
            "UPDATE products SET downloads=downloads+1, last_download=SYSDATE() WHERE product_code=?;",
            [$productCode]);
    }
}
