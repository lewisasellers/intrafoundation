<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductLinks extends Model
{
    protected $table = 'product_links';
    public $timestamps = true;
    protected $primaryKey = 'link_id';

    public static function getById(int $linkId): array
    {
        $links = DB::select("SELECT url FROM product_links WHERE link_id=?;", [$linkId]);
        return $links;
    }

    public static function updateViews($linkId)
    {
        DB::update(
            "UPDATE product_links SET views=views+1 WHERE link_id=?", [$linkId]
        );
    }

    public static function getCategoryId(string $categoryId): string
    {
        if (is_object($categoryId)) {
            $product = $categoryId;
            if (is_numeric($product->categoryId)) {
                $categoryId = $product->categoryId;
            }
        }
        return $categoryId;
    }

    public static function getByTags(string $tag, int $limit = 0): array
    {
        $links = DB::select("SELECT link_id, title, views, url
                FROM product_links
                WHERE tags LIKE ?
                ORDER BY title ASC;",
            [DB::raw('%$tag%')]);
        return $links;
    }

    public static function getByProductCode(string $productCode, int $limit = 0): array
    {
        $links = DB::select("SELECT link_id, title, views, url
                FROM product_links
                WHERE product_code=?
                ORDER BY title ASC;",
            [$productCode]);
        return $links;
    }

}