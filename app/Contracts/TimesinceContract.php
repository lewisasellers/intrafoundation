<?php namespace App\Contracts\Timesince;
/*
*
*/
interface TimesinceContract
{
    public static function init();
    public static function set_tz($tz);
    public static function get_tz();
    public static function show_after1970($timestamp, $default = null);
    public static function show($timestamp, $default = null, $sync_tz = null);
}