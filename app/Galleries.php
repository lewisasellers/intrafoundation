<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Galleries extends Model
{
    protected $table = 'galleries';
    public $timestamps = true;
    protected $primaryKey = 'photograph_id';

}