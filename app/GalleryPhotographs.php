<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GalleryPhotographs extends Model
{
    protected $table = 'gallery_photographs';
    public $timestamps = true;
    protected $primaryKey = 'photograph_id';

    public static function getGalleryId(mixed $galleryId): int
    {
        if (is_object($galleryId)) {
            $product = $galleryId;
            if (is_numeric($product->gallery_id)) {
                $galleryId = $product->gallery_id;
            }
        }
        return $galleryId;
    }

    private static function addImagePath(array $photographs = null)
    {
        $photographs = array_map(function ($photograph) {
            $originalImagePath = public_path() . "/content/gallery/original/{$photograph->filename}";
            $photograph->originalImagePath = file_exists($originalImagePath) ? $originalImagePath : null;
            $photograph->originalImageUrl = file_exists($originalImagePath) ?  "/content/gallery/original/{$photograph->filename}" : null;

            $a = explode(".", $photograph->filename);
            array_pop($a);
            $a[] = "jpg";
            $filename = implode(".", $a);

            $filePath = public_path() . "/content/gallery/{$filename}";
            $photograph->filePath = file_exists($filePath) ? $filePath : null;

            $photograph->imageUrl = file_exists($filePath) ? "/content/gallery/{$filename}" : null;
            $photograph->lightboxUrl = file_exists($filePath) ? "/content/gallery/lightbox/{$filename}" : null;

            return $photograph;
        }, $photographs);

        return $photographs;
    }

    /**
     * @param mixed $galleryId
     * @param int $limit
     * @return array
     */
    public static function getById(int $galleryId = null, int $limit = 0): array
    {
        if ($galleryId == null) return [];
        $photographs = DB::select("SELECT p.photograph_id, p.filename
                FROM gallery_photographs
                p WHERE p.gallery_id=?
                ORDER BY p.arrangement ASC;",
            array($galleryId));
        $photographs = self::addImagePath($photographs);
        return $photographs;
    }

}