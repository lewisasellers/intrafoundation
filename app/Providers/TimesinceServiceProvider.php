<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TimesinceServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register with the IoC Service Container
     *
     * @return void
     */
    public function register()
    {
        //$ts=new \Intrafoundation\Timesince(); $this->app->bind('timesince', $ts );
        /*
        $this->app->bind('App\Providers\Timesince', function ($app) {
            return new App\Providers\Timesince();
        });
        */
//        $this->app->singleton('timesince', \App\Providers\Timesince::class);

        $this->app->singleton('App\Timesince', function ($app) {
            return new \App\Providers\Timesince();
        });

        //$i = app(\App\Providers\Timesince::class);
    }

    /**
     * The services that this service provider registers
     *
     * @return array
     */
    public function provides()
    {
        return ['App\Providers\Timesince'];
    }
}
