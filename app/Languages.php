<?php namespace App;
/**
  Simple model class to handle preferred language selection.
  Used mainly with messages class.
 *
 */
class Languages
{
    // -------------------------------------------------------------------------------------------
    private static $languages=array(
        'en'=>'English',
        'es'=>'Espanol',
        'ru'=>'Russian',
        'cn'=>'Chinese',
        'fr'=>'French'
    );
    private static $preferred=array(
        'en'
    );
    // -------------------------------------------------------------------------------------------

    /*

     * @author		Lewis A. Sellers
     * @date: 2013-3-29
     */
    public static function set_preferred($languages)
    {
        if(!is_array($languages)) $languages=explode(',', $languages);
        self::$preferred=$languages;
    }
    // -------------------------------------------------------------------------------------------

    /*

     * @author		Lewis A. Sellers
     * @date: 2013-3-29
     */
    public static function get_preferred($supported_languages=null)
    {
        if($supported_languages==null) return self::$preferred[0];

        if(!is_array($supported_languages))
                $supported_languages=explode(',', $supported_languages);

        foreach(self::$preferred as $code)
        {
            if(in_array($code, $supported_languages)) return $code;
        }

        return self::$preferred[0];
    }
    // -------------------------------------------------------------------------------------------
}
