<?php namespace App\Http\Controllers;

/**
 * This is a text.
 */
use App\Http\Controllers\Controller;
use DB;
use View;

//use Intrafoundation\Timesince;

use App\Products;
use App\Comments;
use App\Message;
use App\ContactUs;
use Intrafoundation\JSONEndpointDocumentation;

class APIController extends Controller
{
    public $restful = true;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //  $this->middleware('auth:api');
        //    $this->categories=Products::get_categories();
    }

    /**
     * @URL
     * @semanticVersion 1.0.1,
     * @title Index
     * @description Lorem Ipsum,
     * @method GET
     * @urlParams
     * @required
     * @optional
     * @dataParams
     * @successResponse Code: 200 Content: { id: 1, name: "Lorem" }
     * @successResponse Code: 200 Content: { id: 1, name: "Lorem", body: "Lorem Ipsum" }
     * @errorResponse Code: 401 UNAUTHORIZED Content: { error: "Lorem Ipsum" }
     * @sampleCall curl http://example.com/api/news
     * @notes Lorem Ipsum
     * @cached No
     * @authentication NTLM-Digest
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        //    $class2 =__CLASS__;
//        $class3=get_class($this);
        static $results;
        if ($results == null) {
            $o = new JSONEndpointDocumentation();
            $results = $o->createJSONForAllClassFunctions(__CLASS__,"api/");
        }

        return response()
            ->json((object)$results);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @semanticVersion 1.0.0
     * @title News
     * @successResponse Code: 200 Content: { id: 1, name: "Lorem" }
     * @successResponse Code: 200 Content: { id: 1, name: "Lorem", body: "Lorem Ipsum" }
     * @errorResponse Code: 401 UNAUTHORIZED Content: { error: "Lorem Ipsum" }
     */
    public function news()
    {
        $posts = Comments::getList(100, "news");
        return response()
            ->json($posts);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function comments()
    {
        $posts = Comments::getList(100, "all");
        return response()
            ->json($posts);

    }

}
