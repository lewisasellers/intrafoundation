<?php namespace App\Http\Controllers;

use App\Timesince;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

//use Illuminate\Http\Request;
//use Intrafoundation\Timesince;

use App\Products;
use App\Comments;
use App\Message;
use App\ContactUs;
use App\Galleries;
use App\GalleryPhotographs;
use App\ProductLinks;


class NewsController extends BaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $posts = Comments::getList(20, "news");
        $this->formatTimesince($posts,['created_at']);
        return View::make('news.index')
            ->with('meta_title', 'News')
            ->with('posts', $posts);
    }

    /**
     * @param int|null $id
     * @return mixed
     */
    public function view(int $id = null)
    {
        $posts = Comments::getById($id);
        if (count($posts) == 0)
            return Response::view('error.404',
                array('<p class=warning>That post/thread does not seem to exist anymore. Please try again.</p>'),
                404);

        $this->formatTimesince($posts,['created_at']);

        return View::make('news.view')
            ->with('meta_title', $posts[0]->title)
            ->with('posts', $posts)
            ->with('id', $id);
    }

}
