<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;


use App\Products;
use App\Comments;
use App\Message;
use App\ContactUs;
use App\Galleries;
use App\GalleryPhotographs;
use App\ProductLinks;


class PublicController extends BaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
    }

    public function home()
    {
        return view('public.home');
    }

    public function index()
    {
        $comments = Comments::getList(20);
        $downloads = Products::getDownloads(3);
        $products = Products::getList(3);

        $this->formatTimesince($products,['build_date','last_download']);

        return View::make('public.index')
            ->with('meta_title', 'Home')
            ->with('comments', $comments)
            ->with('downloads', $downloads)
            ->with('products', $products);
    }


    public function about_us()
    {
        return View::make('public.about-us')
            ->with('meta_title', 'About Us');
    }


    public function contact_us()
    {
        return View::make('public.contact-us')
            ->with('meta_title', 'Contact Us');
    }


    public function contact_us_post()
    {
        $from_email = Config::get('social.contact-us.to');
        $from_name = Config::get('social.contact-us.name');
        $to_email = Request::get('email');
        $to_name = Request::get('name');
        $subject = 'Contact Form';
        $data = array(
            'data' => Request::get('comment')
        );

        $contact_us = new ContactUs;
        $contact_us->name = $to_name;
        $contact_us->email = $to_email;
        $contact_us->comment = $data['data'];
        $contact_us->save();

        if ($to_email != '') {
            Mail::send('emails.contact', $data,
                function ($message) use ($to_email, $to_name, $from_email, $from_name, $subject) {
                    $message->to($to_email, $to_name);
                    $message->from($from_email, $from_name);
                    $message->subject($subject);
                });
        }

        return View::make('public.contact-us-post')
            ->with('meta_title', 'Contact Us');
    }

    public function sitemap_xml()
    {
        $categories = Products::get_categories();
        $products = DB::select("SELECT product_code FROM products;");
        $comments = DB::select("SELECT id FROM comments;");
        return View::make('public.sitemap_xml')
            ->with('products', $products)
            ->with('comments', $comments)
            ->with('categories', $categories);
    }

}
