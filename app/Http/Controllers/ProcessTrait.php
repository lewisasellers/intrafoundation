<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

//use Illuminate\Http\Request;
//use Intrafoundation\Timesince;

use Intrafoundation\Timesince;

//
trait ProcessTrait
{
    public static $default_timeout = 30;

    //$this->io = new ConsoleOutput();
//$this->io = new SymfonyStyle($this->input, $this->output);

    /**
     *
     * @param type $command
     * @param type $timeout
     * @param type $sleep
     * @return boolean
     */
    public function PsExecute($command, $timeout = null, $sleep = 1)
    {
        if (!is_numeric($timeout)) {
            $timeout = self::$default_timeout;
        }

        Log::error("PsExecute: timecount=$timeout");

        $pid = self::PsExec($command);
        if ($pid === false) {
            return false;
        }

        $timecount = 0;
        Log::error("timecount=$timecount $timeout=$timeout");

        while ($timecount < $timeout) {
            sleep($sleep);
            $timecount += $sleep;
            Log::error("   ** sleep=$sleep timecount=$timecount");

            if (!self::PsExists($pid)) {
                return true;
            }
        }

        self::PsKill($pid);
        return false;
    }

    /**
     *
     * @param type $commandJob
     * @return boolean
     */
    public function PsExec($commandJob)
    {
        Log::error("PsExec($commandJob)");

        $op = "";
        $command = $commandJob . ' > /dev/null 2>&1 & echo $!';
        exec($command, $op);
        $pid = (int)$op[0];

        if ($pid != "") {
            return $pid;
        }

        return false;
    }

    /**
     *
     * @param type $pid
     * @return boolean
     */
    public function PsExists($pid)
    {
        Log::error("PsExists($pid)");

        $output = "";
        exec("ps ax | grep $pid 2>&1", $output);
        while (list(, $row) = each($output)) {

            $row_array = explode(" ", trim($row));

            $check_pid = $row_array[0];
            Log::error("pid $pid == check_pid $check_pid");
            if ($pid == $check_pid) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     * @param type $pid
     */
    private function PsKill($pid)
    {
        Log::error("PsKill($pid)");

        $output = "";
        exec("kill -9 $pid", $output);
    }

}
