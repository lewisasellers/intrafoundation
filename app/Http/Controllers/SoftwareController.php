<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

//use Illuminate\Http\Request;
//use Intrafoundation\Timesince;

use App\Products;
use App\Comments;
use App\Message;
use App\ContactUs;
use App\Galleries;
use App\GalleryPhotographs;
use App\ProductLinks;


class SoftwareController extends BaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
    }


    /**
     * @return mixed
     */
    public function index()
    {
        return $this->category('all');
    }


    /**
     * @param $category_id
     * @return mixed
     */
    public function category($category_id)
    {
        if ($category_id == '') $category_id = 'all';

        if ($category_id == 'all') $category = Products::get_category('all');
        else $category = Products::get_category($category_id);

        $products = Products::getByCategory($category_id);

        $this->formatTimesince($products,['build_date','last_download']);

        $product = end($products);

        $photographs = GalleryPhotographs::getById($product->gallery_id);
        $links = ProductLinks::getByTags($category_id);
        $comments = Comments::getByProductCode($product->product_code);

        $settings = \Config::get('settings');
        $ext = $settings['products']['image']['ext'];
        $stages = Products::get_stages();

       /* foreach ($products as &$product) {
            $filepath = 'content/product_images/' . $product->product_id . '.' . $ext;
            if (file_exists(base_path() . $filepath))
                $product['filepath'] = $filepath;
        }*/

        return View::make('software.category')
            ->with('meta_title', $category->title)
            ->with('long_title', $category->long_title)
            ->with('category', $category)
            ->with('products', $products)
            ->with('comments', $comments)
            ->with('photographs', $photographs)
            ->with('links', $links)
            ->with('settings', $settings)
            ->with('ext', $ext)
            ->with('stages', $stages);
    }

    /**
     * @return mixed
     */
    public function link()
    {
        $link_id = Request::get('link_id');

        $links = ProductLinks::getById($link_id);
        if (count($links) == 0)
            return Response::view('error.404',
                array('<p class=warning>Invalid link.</p>'), 404);
        $link = end($links);

        ProductLinks::updateViews($link_id);

        header("Location: {$link->url}");
        exit;
    }

    /**
     * @param string $product_code
     * @return mixed
     */
    public function product(string $product_code)
    {
        $products = Products::getByProductCode($product_code, 1);
        if (count($products) == 0)
            return Response::view('error.404', array('<p class=warning>?</p>'), 404);

        $this->formatTimesince($products,['build_date','last_download']);

        $product = end($products);

        //
        $product = (array)$product;
        $size = 0;
        $file = $product['file'];
        if (strlen($file) > 0) {
            $filepath = public_path() . '/content/products/' . $file;
            if (file_exists($filepath)) {
                $size = filesize($filepath);
            }
        }
        $product['size'] = $size;
        $product['sizeKb'] = ($size / 1024);
        $product = (object)$product;

        $photographs = GalleryPhotographs::getById($product->gallery_id);
        $links = ProductLinks::getByProductCode($product->product_code);
        $comments = Comments::getByProductCode($product->product_code);

        Products::updateViewsByProductId($product->product_id);

        $settings = Config::get('settings');
        $ext = $settings['products']['image']['ext'];
        $stages = Products::get_stages();
        $filepath = '/content/product_images/' . $product->product_id . '.' . $ext;

        $version = Products::printVersion($product->version_type, $product->version);

        return View::make('software.product')
            ->with('meta_title', $product->name)
            ->with('product', $product)
            ->with('photographs', $photographs)
            ->with('comments', $comments)
            ->with('links', $links)
            ->with('filepath', $comments)
            ->with('product_code', $product->product_code)
            ->with('settings', $settings)
            ->with('ext', $ext)
            ->with('stages', $stages)
            ->with('filepath', $filepath)
            ->with('version', $version);
    }


    /**
     * @param $product_code
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download($product_code)
    {
        $products = Products::getByProductCode($product_code);
        if (count($products) == 0)
            return Response::view('error.404',
                array('<p class=warning>Product not found</p>'), 404);
        $this->formatTimesince($products,['build_date','last_download']);
        $product = end($products);

        $filename = $product->file;

        Products::updateViewsByProductCode($product_code);

        $file = public_path() . "/content/products/{$filename}";
        if (!file_exists($file))
            return Response::view('error.404', ['<p class=warning>File not found</p>'], 404);

        $headers = array(
            'Content-Type: application/octet-stream',
            'Content-Type: application/force-download',
            'Content-Type: application/download',
            'Content-Transfer-Encoding: binary',
            //"Pragma: no-cache",
            //"Expires: 0"
        );
        /*
     header("Content-disposition: attachment; filename=\"$filename\"");
     header("Content-Length: ".filesize($filepath));
     */

        //  return Response::download($file, $filename, $headers);
        return response()->download($file, $filename, $headers);
    }


    /**
     * @param $product_code
     * @return mixed
     */
    public function software_product_manual($product_code)
    {
        $products = DB::select("SELECT * FROM products WHERE product_code=?;",
            array($product_code));
        if (count($products) == 0)
            return Response::view('error.404', array('<p class=warning>?</p>'), 404);
        $this->formatTimesince($products,['build_date','last_download']);
        $product = end($products);

        $photographs = DB::select("SELECT s.photograph_id, s.gallery_id, p.description, p.filename
    FROM gallery s,gallery_photographs p
    WHERE s.product_code=? AND s.gallery_id=p.gallery_id;",
            array($product->product_code));

        DB::update("UPDATE products SET views=views+1 WHERE product_id=?;",
            [$product->product_id]);

        return View::make('software.software_manual')
            ->with('meta_title', $product->name)
            ->with('product', $product)
            ->with('photographs', $photographs)
            //->with('links', $links)
            // ->with('comments', $comments)
            ;
    }

}
