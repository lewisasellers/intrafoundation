<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

use App\Products;
use App\Comments;
use App\Message;
use App\ContactUs;
use App\Galleries;
use App\GalleryPhotographs;
use App\ProductLinks;

use App\Timesince;
//use App\Providers\Timesince;

class BaseController extends Controller
{
    use S3Trait;
    use TimesinceTrait;
    use ElasticCacheTrait;

    private $timesince;

    /**
     * The layout that should be used for responses.
     */
    protected $layout = 'layouts.default';
    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    /* protected function setupLayout()
    {
    if ( ! is_null($this->layout))
    {
    $this->layout = View::make($this->layout);
    }
    } */

    //$this->io = new ConsoleOutput();
//$this->io = new SymfonyStyle($this->input, $this->output);
//     //$i = app(\App\Providers\Timesince::class);
    public function __construct(\App\Providers\Timesince $timesince)
    {
        $this->timesince=$timesince;

        $this->__S3Trait_construct();
       // $this->__ElasticCacheTrait_construct();

        $this->categories = Products::get_categories();
        View::share('categories', $this->categories);
        View::share('CDN', $this->get_prefix());
    }
}