<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

//use Illuminate\Http\Request;

use Intrafoundation\Timesince;


trait TimesinceTrait
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
 /*   public function __TimeSinceTrait_construct()
    {
      // View::share('categories', $this->categories);

        $t = resolve('Timesince');
        print_r($t);
        $t->hello('worlds');

        //ti $t2=resolve('TimesinceFacade');
        $t2 = resolve('timesince');
        print_r($t2);
        $t2->hello('worlds2');

        // echo "<br>";
        // static::$app->make('timesince')->hello($str);
        // resolve('timesince')->hello($str);

    }*/


    protected static function formatTimesince(array $rows = null, array $fields): array
    {
        Timesince::init();
        $rows = array_map(function ($row) use ($fields) {
            foreach($fields as $field) {
                $row->$field = Timesince::show($row->$field);
            }
            return $row;
        }, $rows);
        return $rows;
    }

}
