<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

use Intrafoundation\Timesince;

use Aws\S3\ElastiCacheClient;

//
trait ElasticCacheTrait
{
    private $default_timeout = 30;
  //  private $s3_prefix = "https://s3.amazonaws.com";
  //  private $bucket;
    private $_ElastiCacheClient;

    public function __ElastiCacheTrait_construct()
    {
        //  parent::__construct();

        //$this->bucket = env('AWS_S3_BUCKET');

        $this->_ElastiCacheClient = ElastiCacheClient::factory(array(
            'key' => env('AWS_KEY'),
            'secret' => env('AWS_SECRET'),
            'version' => '2006-03-01',
            'region' => env('AWS_REGION'),

        ));

    }


    /**
     * Copy a file to the S3 image bucket
     * @param string $pathToFile
     * @param string $basename
     * @return bool success
     */
    protected function update_cache_item($pathToFile, $basename = null)
    {
        try {
            if (!$basename) {
                $basename = basename($pathToFile);
            }

            $result = $this->_S3Client->putObject(array(
                'Bucket' => $this->bucket,
                'Key' => $basename,
                'SourceFile' => $pathToFile,
                'Metadata' => []
            ));

            // We can poll the object until it is accessible
            $this->_S3Client->waitUntil('ObjectExists', array(
                'Bucket' => $this->bucket,
                'Key' => $basename
            ));

            return $result;
        } catch (Exception $e) {
            $this->_messages->throw_error($e->getMessage());
        }

        return false;
    }

    /**
     * List S3 images in bucket
     * @param string $filename
     * @return bool success
     */
    public function list_cache_items()
    {
        $iterator = $this->_S3Client->getIterator('ListObjects', array(
            'Bucket' => $this->bucket
        ));

        $retList = [];
        foreach ($iterator as $object) {
            $retList[] = $object['Key'];
        }
        return $retList;
    }

    /**
     *
     * @param type $pathToFile
     * @param type $basename
     * @return type
     */
    protected function get_cache_item($pathToFile, $basename)
    {
        return $this->_S3Client->getObject(array(
            'Bucket' => $this->bucket,
            'Key' => $basename,
            'SaveAs' => $pathToFile
        ));
    }

    /**
     *
     * @param type $names
     * @return type
     */
    protected function delete_cache_item($names)
    {
        if (!is_array($names)) {
            $names = [$names];
        }
        $objList = [];
        foreach ($names as $k) {
            $objList[] = ['Key' => $k];
        }
        $result = $this->_S3Client->deleteObjects(array(
            'Bucket' => $this->bucket,
            'Objects' => $objList,
            //'Quiet' => true,
        ));

        return $result;
    }

}
