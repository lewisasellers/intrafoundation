<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

//use Illuminate\Http\Request;
//use Intrafoundation\Timesince;

use App\Products;
use App\Comments;
use App\Message;
use App\ContactUs;
use App\Galleries;
use App\GalleryPhotographs;
use App\ProductLinks;


class CommentsController extends BaseController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
    }

    public function index()
    {
        $comments=Comments::getAll();

        $this->formatTimesince($comments,['created_at']);

        return View::make('comments.index')
            ->with('meta_title', 'Comments')
            ->with('comments', $comments)
            ->with('source', Comments::getSources());
    }


    public function view($id = null)
    {
        $comments = Comments::getById($id);
        if (count($comments) == 0)
            return Response::view('error.404',
                array('<p class=warning>That post/thread does not seem to exist anymore. Please try again.</p>'),
                404);
        $this->formatTimesince($comments,['created_at']);

        return View::make('comments.view')
            ->with('meta_title', $comments[0]->title)
            ->with('comments', $comments)
            ->with('id', $id);
    }


    public function view_thread($thread_id = null)
    {
        $comments = DB::select("SELECT c.*,u.name FROM comments c
    LEFT JOIN users u ON u.user_id=c.user_id
    WHERE c.thread_id=? OR c.id=? ORDER BY c.created_at DESC;",
            [$thread_id, $thread_id]);
        $this->formatTimesince($comments,['created_at']);

        if (count($comments) == 0)
            return Response::view('error.404',
                array('<p class=warning>That post/thread does not seem to exist anymore. Please try again.</p>'),
                404);

        return View::make('comments.comments_view')
            ->with('meta_title', $comments[0]->title)
            ->with('comments', $comments)
            ->with('id', 0)
            ->with('thread_id', $thread_id);
    }


    public function product_code($product_code)
    {
        $comments = DB::select("SELECT c.* FROM comments c
    LEFT JOIN users u ON u.user_id=c.user_id
    WHERE c.product_code=? ORDER BY c.created_at DESC;",
            [$product_code]);
        $this->formatTimesince($comments,['created_at']);

        if (count($comments) == 0)
            return Response::view('error.404',
                array('<p class=warning>That post/thread does not seem to exist anymore. Please try again.</p>'),
                404);

        $id = $comments[0]->id;

        return View::make('comments.comments_view')
            ->with('meta_title', $comments[0]->title)
            ->with('comments', $comments)
            ->with('id', $id)
            ->with('product_code', $product_code);
    }

}
