<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

use Intrafoundation\Timesince;

use Aws\Ses\SesClient;

//
trait SeSTrait
{
    //  private static $default_timeout = 30;
    //  private static $s3_prefix = "https://s3.amazonaws.com/";
    private $_SeS;

    private $bucket;

    //$this->io = new ConsoleOutput();
//$this->io = new SymfonyStyle($this->input, $this->output);

    public function __construct()
    {
        parent::__construct();

        $this->_SeS = SesClient::factory(array(
            'region' => env('AWS_REGION'),
            'version' => '2010-12-01',
            'key' => env('AWS_KEY'),
            'secret' => env('AWS_SECRET'),
        ));


    }

}
