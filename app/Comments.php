<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Comments extends Model
{
    protected $table = 'comments';
    public $timestamps = true;
    protected $primaryKey = 'id';

    private static $sourceTypes = array(
        'forum' => 'Forum',
        'product' => 'Product Comment',
        'news' => 'News',
        'faq' => 'FAQ',
        'tutorial' => 'Tutorial',
        'photograph' => 'Photograph',
        'comment' => 'Product Comment',
    );

    public static function getSources()
    {
        return self::$sourceTypes;
    }

    public static function getProductCode(mixed $productCode): int
    {
        if (is_object($productCode)) {
            $product = $productCode;
            if (is_numeric($product->productCode)) {
                $productCode = $product->productCode;
            }
        }
        return $productCode;
    }

    private static function addTitle(array $comments = null)
    {
        $comments = array_map(function ($comment) {

            if (strlen($comment->product_code) > 0)
                $comment->title = "\"{$comment->product_code}\"";
            else
                $comment->title = htmlentities($comment->title);
            if (strlen($comment->title) == 0) {
                $comment->title = "[No title]";
            }

            return $comment;
        }, $comments);

        return $comments;
    }


    private static function addReadMore(array $comments = null)
    {
        $comments = array_map(function ($comment) {
            $comment->read_me = substr($comment->message, 0, 132);
            return $comment;
        }, $comments);
        return $comments;
    }


    private static function addSearchUrl(array &$comments)
    {
        foreach ($comments as &$comment) {
            if (
                strtolower(substr($comment->url, 0, 7)) == 'http://'
                || strtolower(substr($comment->url, 0, 8)) == 'https://'
                || strtolower(substr($comment->url, 0, 6)) == 'ftp://'
                || strtolower(substr($comment->url, 0, 9)) == 'mailto://'
                || strtolower(substr($comment->url, 0, 7)) == 'news://'
            ) {
                $searchUrl = $comment->url;
            } else {
                $searchUrl = 'http://www.google.com/search?q=' . $comment->url;
            }
            $comment = (array)$comment;
            $comment['searchUrl'] = $searchUrl;
            $comment = (object)$comment;
        }

        return $comments;
    }

    private static function addSourceType(array $comments = null)
    {
        $comments = array_map(function ($comment) {
            $comment->source_type = isset($sources[$comment->source]) ? $sources[$comment->source] : 'Unknown source of ' . $comment->source;
            return $comment;
        }, $comments);
        return $comments;
    }


    public static function getAll(int $limit = 20, string $type = "news")
    {
        $comments = DB::select("SELECT c.*,u.name, message
        FROM comments c
        LEFT JOIN users u ON u.user_id=c.user_id
        WHERE c.id=c.thread_id OR c.thread_id=0
        ORDER BY c.created_at DESC;");

        $comments = self::addTitle($comments);
        $comments = self::addSearchUrl($comments);
        $comments = self::addSourceType($comments);
        $comments = self::addReadMore($comments);
        return $comments;
    }

    public static function getList(int $limit = 20, string $type = "news")
    {
        $comments = DB::select("SELECT c.*
        FROM comments c WHERE source=\"news\"
        ORDER BY created_at DESC LIMIT {$limit}");
        $comments = self::addTitle($comments);
        $comments = self::addSearchUrl($comments);
        $comments = self::addSourceType($comments);
        $comments = self::addReadMore($comments);
        return $comments;
    }

    public static function getNews($limit = 0)
    {
        return self::getList($limit, "news");
    }


    public static function getByProductCode(string $productCode = null, $limit = 0)
    {
        if ($productCode == null) return [];

        $comments = DB::select("SELECT *
                FROM comments
                WHERE product_code=?
                ORDER BY created_at DESC;",
            [$productCode]);
        $comments = self::addTitle($comments);
        $comments = self::addSearchUrl($comments);
        $comments = self::addSourceType($comments);
        $comments = self::addReadMore($comments);
        return $comments;
    }

    public static function getById(int $id)
    {
        $comments = DB::select("SELECT c.*,u.name
        FROM comments c
        LEFT JOIN users u ON u.user_id=c.user_id
        WHERE c.id=? OR c.thread_id=?
         ORDER BY c.created_at DESC;",
            [$id, $id]);
        $comments = self::addTitle($comments);
        $comments = self::addSearchUrl($comments);
        $comments = self::addSourceType($comments);
        $comments = self::addReadMore($comments);
        return $comments;
    }

}