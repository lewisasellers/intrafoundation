<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use \DB; //Illuminate\Support\Facades\DB

class ContactUs extends Model
{
    protected $table = 'contact_us';
    public $timestamps = true;
}
