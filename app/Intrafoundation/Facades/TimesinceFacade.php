<?php namespace Intrafoundation\Facades;

use Illuminate\Support\Facades\Facade;

class TimesinceFacade extends Facade {
    /**
    * Get the binding in the IoC container
    *
    * @return string
    */
    protected static function getFacadeAccessor()
    {
        return 'Timesince'; // the IoC binding.
    }
    
  /*  protected static function helloWorld()
    {
        echo "Hello World";
    }
        protected static function helloWorld2(string $str)
    {
        echo "Hello World $str";
    }*/
    public static function show(string $date)
    {
    //   echo "TimesinceFacade::show [$date] ";
        //static::$app->make('timesince')->show($date);
      resolve('Timesince')->show($date);
       // $this->show($date);
    }

    public static function hello(string $str)
    {
       echo "TimesinceFacade::hello [ $str ] <br>";
      //// print_r(static::$app->make('timesince'));
      // echo "<br>";
    // static::$app->make('timesince')->hello($str);
   //resolve('Timesince')->hello($str);
      // echo "end TimesinceFacade::hello ";
       // $this->show($date);
    }
}