<?php namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

use App\Contracts\Timesince\TimesinceContract;
use App\Languages;

//use Illuminate\Http\Request;
//use Intrafoundation\Timesince;

/**
 *
 * This is a stream-lined date printing class that has only on formatting option: show.
 * It prints a date showing relative time from web servers current time
 * and the supplied timedate stamp.
 *
 */
class Timesince implements TimesinceContract
{
    private static $TIMEZONE = null;
    private static $language = 'en';

    public function __construct()
    {
        self::init();
    }

    /**
    *  change to TZ http://www.theprojects.org/dev/zone.txt
     *
     */
    public static function init()
    {
        $zonelist = array('Kwajalein' => -12.00, 'Pacific/Midway' => -11.00, 'Pacific/Honolulu' => -10.00, 'America/Anchorage' => -9.00, 'America/Los_Angeles' => -8.00, 'America/Denver' => -7.00, 'America/Tegucigalpa' => -6.00, 'America/New_York' => -5.00, 'America/Caracas' => -4.30, 'America/Halifax' => -4.00, 'America/St_Johns' => -3.30, 'America/Argentina/Buenos_Aires' => -3.00, 'America/Sao_Paulo' => -3.00, 'Atlantic/South_Georgia' => -2.00, 'Atlantic/Azores' => -1.00, 'Europe/Dublin' => 0, 'Europe/Belgrade' => 1.00, 'Europe/Minsk' => 2.00, 'Asia/Kuwait' => 3.00, 'Asia/Tehran' => 3.30, 'Asia/Muscat' => 4.00, 'Asia/Yekaterinburg' => 5.00, 'Asia/Kolkata' => 5.30, 'Asia/Katmandu' => 5.45, 'Asia/Dhaka' => 6.00, 'Asia/Rangoon' => 6.30, 'Asia/Krasnoyarsk' => 7.00, 'Asia/Brunei' => 8.00, 'Asia/Seoul' => 9.00, 'Australia/Darwin' => 9.30, 'Australia/Canberra' => 10.00, 'Asia/Magadan' => 11.00, 'Pacific/Fiji' => 12.00, 'Pacific/Tongatapu' => 13.00);
        $tz = Config::get('settings.TZ');
        $index = array_keys($zonelist, $tz);
        date_default_timezone_set($index[0]); //todo
        self::set_tz($tz);
        self::$language = Languages::get_preferred();
    }

    /**
     * @param $tz
     */
    public static function set_tz($tz)
    {
        self::$TIMEZONE = $tz;
    }


    /**
     * @return null
     */
    public static function get_tz()
    {
        return self::$TIMEZONE;
    }


    /**
     * @param $timestamp
     * @param null $default
     * @return string
     */
    public static function show_after1970($timestamp, $default = null)
    {
        /*if (!is_numeric($timestamp)) $timestamp=strtotime($timestamp);
        $MAX=0; //+(60*60*24);
        if($timestamp<=$MAX)
            return $default;*/
        return self::show($timestamp, $default);
    }

    /**
     * @param $timestamp
     * @param null $default
     * @param null $sync_tz
     * @return string
     */
    public static function show($timestamp, $default = null, $sync_tz = null)
    {
        if (!is_numeric($timestamp)) $timestamp = strtotime($timestamp);

        if (self::$language == 'es')
            $labels = array(
                'unknown' => 'Unknown', 'minutes' => 'minutos hace', 'hours' => 'ayer hace', 'days' => 'días hace', 'never' => 'No'
            );
        else
            $labels = array(
                'unknown' => 'Unknown', 'minutes' => 'minutes ago', 'hours' => 'hours ago', 'days' => 'days ago', 'never' => 'Never'
            );
        if ($timestamp == 0) return $labels['unknown'];

        //
        switch ($sync_tz) {
            case 'database':
                $REF_TIMEZONE = Config::get('settings.timezone_database');
                break;
            case 'filesystem':
                $REF_TIMEZONE = Config::get('settings.timezone_filesystem');
                break;
            case 'server':
                $REF_TIMEZONE = Config::get('settings.timezone_server');
                break;
            default:
                $REF_TIMEZONE = Config::get('settings.timezone_server');
                break;
        }

        if (self::$TIMEZONE === null) self::init();
        $tz = (self::$TIMEZONE - $REF_TIMEZONE);
        $TIMEOFFSET = ($tz * 60 * 60);


        $diff = (time() + $TIMEOFFSET) - $timestamp;
        if ($diff > (60 * 60 * 24 * 30)) {
            return date("Y-m-d h:iA", $timestamp);
        }

//
        $m = $diff / (60);
        if ($m < 60) return (int)($m) . ' ' . $labels['minutes'];

        $h = $m / 60;
        if ($h < 48) return (int)($h) . ' ' . $labels['hours'];

        $d = $h / 30;
        if ($d < 30) return (int)($d) . ' ' . $labels['days'];

        $t = $timestamp + $TIMEOFFSET;

        $s = "";
        {
            $m = date("m", $t);
            $d = date("d", $t);
            $y = date("Y", $t);
            $h = date("G", $t);
            $min = date("i", $t);
        }

        if ($m >= 1 && $m <= 12) $s .= self::printable_month($m);
        $s .= "&nbsp;";
        if ($d >= 1 && $d <= 32) $s .= sprintf('%02d', $d);
        $s .= "&nbsp;";
        if ($y >= 0) $s .= sprintf('%04d', $y);

        $s .= "&nbsp;";

        //
        if ($h >= 1 && $h <= 12) $s .= sprintf('%02d', $h);
        else if ($h > 12 && $h <= 24) $s .= sprintf('%02d', $h - 12);
        else $s .= "12";
        $s .= ":";
        if ($min >= 0 && $min <= 59) $s .= sprintf('%02d', $min);
        else $s .= "60";

        $s .= "&nbsp;";
        if ($h >= 0 && $h <= 11) $s .= "AM";
        else $s .= "PM";

        return $s;
    }


    /**
     * @param $timestamp
     * @return bool
     */
    private static function is_null_date($timestamp)
    {
        return strtotime($timestamp) == 0 ? true : false;
    }

    /**
     * @param $month
     * @return mixed|string
     */
    private static function printable_month($month)
    {
        if (self::$language == 'es')
            $months = array(
                "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio​​", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
            );
        else
            $months = array(
                "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
            );

        $month = (int)$month;
        if ($month < 1 || $month > 12) return '';
        return $months[$month - 1];
    }


    protected static function formatTimesince(array $rows = null, array $fields): array
    {
        self::init();
        $rows = array_map(function ($row) use ($fields) {
            foreach($fields as $field) {
                $row->$field = Self::show($row->$field);
            }
            return $row;
        }, $rows);
        return $rows;
    }
}
