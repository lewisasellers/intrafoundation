<div id="signup-box" style="display:none">
    <div class="col-md-12">

        <p>Signup with Intrafoundation Software</p>

        <form action="/signup" method="post">

            @foreach($errors->all('
              :message
              ') as $message)
                <p class="alert has-error">{{ $message }}<p>
            @endforeach

            <p>
                {{ Form::label('email', 'Email Address') }}
                <input type="text" name="email" id="email" value="{{Input::old('email')}}" placeholder="your@email.com">
            </p>

            <p>
                {{ Form::label('password', 'Password') }}
                <input type="password" name="password" id="password" value="" placeholder="password">
            </p>

            <p>
                <button type="submit">Signup</button>
            </p>
        </form>


    </div>
</div>
