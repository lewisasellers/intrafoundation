<div id="login-box" style="display:none">
    <div class="col-md-12">

        <p>Login to Intrafoundation Software</p>
        <form action="/login" method="post">

            <!-- if there are login errors, show them here -->
            @foreach($errors->all('
              :message
              ') as $message)
                <p class="alert has-error">{{ $message }}<p>
            @endforeach

            <p>
                <label for="email">Email</label><br>
                <input type="text" name="email" id="email" value="{{Request::old('email')}}"
                       placeholder="your@email.com">
            </p>

            <p>
                <label for="password">Password</label><br>
                <input type="password" name="password" id="password" value="" placeholder="password">
            </p>
            <label><input type="checkbox" name="persist" tabindex="3">Remember me</label>
            <p>
                <button type="submit">Login</button>
            </p>
        </form>

    </div>
</div>
