@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-md-6">

            @include('software.partials.product-info')
            @include('software.partials.product-download')

        </div>

        <div class="col-md-3">

            @include('software.partials.product-links')
            @include('software.partials.product-comments')

        </div>
        <div class="col-md-3">

            @include('software.partials.product-gallery')

        </div>
    </div>
@stop

@section('scripts')
@stop
