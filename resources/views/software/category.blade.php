@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-md-8">
            @foreach($products as $product)

                <div class="product">
                    @if($product->imageUrl!=null)
                        <a href="/software/{{$product->product_code}}"><img src="{{$product->imageUrl}}"
                                                                            class="product product-right"></a>
                    @endif

                    <h2><a href="/software/{{$product->product_code}}">{{$product->name}}</a>
                        ({{$product->version}}{{$stages[$product->stage]}}) <span
                                class=tt> {{ $product->build_date}}</span></h2>

                    <div class=info>
                        <small>{{$product->platform}} <b>{{$product->technology}}</b>
                            @if($product->source_code)
                                [w/ {{$product->source_language}} source code]
                            @endif

                            @if($product->obsolete==1)
                                <b>Obsolete</b>
                            @endif
                        </small>
                    </div>


<code>views <b>{{$product->views}}</b> downloads
                        <b>{{$product->downloads}}</b> (last {{$product->last_download}}</code>
                    <br>
                    <br>

                    <p class=description>{{$product->blurb}}</p>
                    <div class="button-line">
                        <a class="button" href="/software/{{$product->product_code}}">view</a>
                    </div>

                </div> <!-- end product -->

            @endforeach

            <br>
            <small>
                <code class="disclaimer">
                    Note: Statistics such as views
                    and downloads were pulled from previous versions of this website.
                    Some items were tracked and some were not. This accounts for the
                    fact that some older items seem to have never been downloaded (they
                    were not being tracked) and downloads are higher than the number of
                    times a product has been viewed (views were not counted until
                    October 2005).
                </code>
            </small>

        </div>

        <div class="col-md-4">
            @if(isset($photo) && strlen($photo)>0)
                <div class=photo><img src="{{CDN}}/images/{{$photo}}" alt=""></div>
                @if(isset($photo_caption) && strlen($photo_caption)>0)
                    <div class="photocaption">{{$photo_caption}}</div>
                @endif
            @endif

            @if(count($links)>0)
                <h3><span class=nobr>Related External Links</span></h3>
                <div class=list>
                    <ul>
                        @foreach($links as $link)
                            <li>
                                <a href="/software-link/?link_id={{$link->link_id}}">{{$link->title}}</a>
                                <small>(views {{$link->views}})</small>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif

        </div>

    </div>
@stop

@section('scripts')
@stop
