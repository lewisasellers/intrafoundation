<h3>Download</h3>

@if ($product->size == 0)
    <p>There is nothing to download.</p>
@else
    <p>We hope you find your download of use.</p>
@endif

<form enctype="application/x-www-form-urlencoded"
      action="/download/{{$product->product_code}}" method="GET">
    <div align=center>
        <input class=download - button type="submit" value="Download"><br>
        @if($product->size> 0)
            <small>({$product->sizekb}} KB)</small>
        @endif
    </div>
</form>

<div align=center>
    <br>
    @if ($product->bits == 'neutral')
        Runs on any bit system, 32, 64, etc
    @else
        {{$product->bits}} bit software
    @endif
</div>

<div align=center><br>
    {{$version}}
    @if ($product->build_date > 0)
        (<code>{{$product->build_date}}</code>)<br>
        <br><code>views <b>{{ $product->views }}</b></code><br>
        <br>
        <small>downloads <b>{{$product->downloads}}</b><br>
            {{$product->last_download}}
        </small>
        <br>
    @endif

</div>
