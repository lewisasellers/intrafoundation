@if (strlen($product->product_code))
    <h2><a href="/software/{{$product_code}}">{{$product->name}}</a>
        ({{$product->version.$stages[$product->stage]}})
        <code>{{$product->build_date}}</code></h2>
@else
    <h2><a href="/software/{{$product_code}}">{{$product->name}}</a>
        ({{$product->version.$stages[$product->stage]}})
        <code>{{$product->build_date}}</code></h2>
@endif

@if (file_exists(public_path().$filepath))
    <img src="{{$filepath}}" class="product" alt="">
@endif
@if ($product->third_party==1)
    <div align=center>THIS IS THIRD PARTY SOFTWARE. IT IS AVAILABLE HERE ONLY AS A CONVENIENCE.
    </div>
@endif

<div align=right>
    <small>product id <b>{{$product->product_id}}</b></small>
</div>

<br><code>Send related correspondence to:
    <a href="mailto:webmaster@intrafoundation.com">webmaster@intrafoundation.com</a>
</code>
<br>
<br>

<div class=info>
    <small>{{$product->platform}} <em>{{$product->technology}}</em></small>
    ({{$product->source_code}}) [w/ {{$product->source_language}} source code]
</div>

<div align=center>
    @if ($product->obsolete)
        <b style="">OBSOLETE</b>
    @endif
    @if ($product->shelved)
        SHELVED
    @endif
    @if ($product->experimental)
        EXPERIMENTAL
    @endif
</div>

@if (strlen($product->description))
    <h3>Description</h3>
    <div>{!! $product->description !!}</div>
@endif
@if (strlen($product->copyright))
    <h3>Copyright</h3>
    <p class=item>{!! $product->copyright !!}</p>
@endif

<!--/* if(strlen($product->manual)>0)
              {
              <h3>Manual</h3>
              if($product->manual_format=="TEXT")
              <div id=manual>".$product->manual."</div>\n\n
              else if($product->manual_format=="HTML")
              {
              <LINK REL=StyleSheet HREF=\"/content/product_code/".$product->product_code."/manual.css\" TITLE=\"Style\" TYPE=\"text/css\">
              <div id=manual>".$product->manual."</div>\n\n
              }
              } */-->

@if (strlen($product->platform_specs))
    <h3>Platform Specifications</h3>
    <p class=item>{{$product->platform_specs}}</p>
@endif

@if ($product->stage > 0)
    <div class=item>
        <a name="omega"><h2>Software Development Stages</h2></a>
        <p>There are four stages in software development: ALPHA, BETA, GAMMA
            and OMEGA.</p>
        <blockquote>
            <p>
                <strong>Alpha</strong> is where programming for the software is still
                heavily ongoing. If you can start up the program without it rebooting
                the computer you're generally happy.
            </p>
            <p>
                <strong>Beta</strong> is where you've gotten a great bulk of the
                program working (sort of) and you're letting other people try it out
                to test if they can use it or crash it.
            </p>
            <p>
                <strong>Gamma</strong> is also sometimes called being "Gone Gold".
                It's publically (or commercially) released software. If it's in a box
                or shrink-wrap it's Gamma (you hope).
            </p>
            <p>
                <strong>Omega</strong> is the absolute final release. Not all
                software publishers do this, but it is when they decide they're never
                going to work on the software again and generally release the entire
                thing for free, sometimes with the full source code.
            </p>
        </blockquote>
    </div>
@endif
<h4 align=right>
<!--<a href="/software/manual/{{$product_code}}">[Manual friendly version]</a>-->
</h4>

@if ($product->pdf == 1)
    <h4 align=right>&gt;&nbsp;&gt;&nbsp;&gt;<a href="/content/pdf/{{$product_code}}.pdf"> [ PDF
            Manual ]</a>
    </h4>
@endif
