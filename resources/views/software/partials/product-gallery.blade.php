<h3>Gallery</h3>

@if ($product->gallery_id == 0 || count($photographs) == 0)
    <p>There is no gallery currently available.</p>
@else
    @foreach ($photographs as $photograph)
        <div align=center>
            <a rel="lightbox[{{$product->product_id}}]" title="{{$photograph->filename}}"
               href="{{$photograph->lightboxUrl}}">
                <img src="{{$photograph->imageUrl}}" class="product-thumbnail">
            </a>
        </div>

    @endforeach
@endif

