@if (isset($links) && count($links) > 0)
    <h3>Related External Links</h3>
    <ul>
        @foreach ($links as $link)
            <li>
                <a href="/software-link/?link_id={{$link->link_id}}">{{$link->title}}</a>
                <small>(views {{$link->views}})</small>
            </li>
        @endforeach
    </ul>
@endif