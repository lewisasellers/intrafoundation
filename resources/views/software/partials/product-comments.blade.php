<h3>Comments</h3>
@if(Auth::check())
    <div class="button-line">
        <a class=button href="/comments/reply?product_code={{$product_code}}">Leave a
            comment</a>&nbsp;&nbsp;&nbsp;
    </div>
@endif

@if (!isset($comments) || count($comments) == 0)
    <br>
    <p>There are no comments about this product. Be the first to leave a comment.</p>
@else
    @foreach ($comments as $comment)
        <div class=comment>
            <b>{{$comment->author}}</b><br>
            <span class=tt>{{$comment->created_at}} </span>
            <br>
            <br>
            <p>
                {{ $comment->message }}
            </p>

        @if (strlen( $comment->url))
            <!--if (
                        strtolower(substr( $comment->url, 0, 7)) == 'http://'
                        || strtolower(substr( $comment->url, 0, 8)) == 'https://'
                        || strtolower(substr( $comment->url, 0, 6)) == 'ftp://'
                        || strtolower(substr( $comment->url, 0, 9)) == 'mailto://'
                        || strtolower(substr( $comment->url, 0, 7)) == 'news://'
                        ) {
                        $outurl =  $comment->url;
                        } else {
                        $outurl = 'http://www.google.com/search?q=' .  $comment->url;
                        }
                        <a rel=\"follow\" href=\"$outurl\"> $comment->url</a><br>
                        -->
            @endif

            <div class="id">{{$comment->id}},{{$comment->thread_id}}</div>
            @if(Auth::check())
                <div class="button-line">
                    <a class=button
                       href="/comments/reply?product_code={{$product->product_code}}">Reply to
                        comment</a>&nbsp;&nbsp;&nbsp;
                </div>
            @endif

        </div>

    @endforeach

    <br>
    @if(Auth::check())
        <div class="button-line">
            <a class=button href="/comments/reply?product_code={{$product->product_code}}">Leave a
                comment</a>
        </div>
    @endif

    <br>
    <div class="button-line">
        <a class=button href="/search/?q={{ $product->product_code}}"> View all product
            comments / replies </a>
    </div>
@endif