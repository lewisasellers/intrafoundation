@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <h1>{{$meta_title}}</h1>
        <p>
            News about the site, the software on it and the community of people surrounding all of this.
        </p>

        @if (count($posts)==0)
            <br><br>
            <div align=center>There is currently no news available.</div>
        @endif

        @foreach ($posts as $post)
            <div class="comment">
                <a name="{{ $post->id }}"></a>
                <h2>
                    <a href="/news/{{$post->id}}">{{ htmlentities($post->title) }}
                    </a>
                </h2>
                <small class="tt"> {{ $post->created_at }}</small>
                <br>
                <p>
                    {{$post->message}}
                </p>
            </div>
        @endforeach

    </div>
@stop

@section('scripts')
@stop
