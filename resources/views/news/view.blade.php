@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div align=right>
                <a class=button href="/news/">List all</a>&nbsp;&nbsp;&nbsp;
                @if(Auth::check())
                    <a class=button href="/news/add" rel="nofollow">New Post</a>
                @endif
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            @foreach($posts as $post)

                <div class="comment">

                    @if(strlen($post->product_code)>0)
                        <code>Comment about: <a href="/software/{{$post->product_code}}">
                                {{$post->product_code}}</a></code>
                        (<i><a href="/comments/product-code/{{$post->product_code}}">All comments</a></i>)<br>
                    @endif

                    @if($post->source=="Comment")
                        <h2><span>{{$post->source}}</span>
                            <small>View</small>
                            <a href="/software/{{$post->product_code}}">{{$post->title}}</a></h2>
                    @endif

                    @if($post->source=="comments")
                        <h2><span>{{$post->source}}</span> <a href="/comments/view/{{$id}}">{{$post->title}}</a></h2>
                    @else
                        <h2>{{htmlentities($post->title)}}</h2>
                    @endif

                    @if(strlen($post->name)>0)
                        <code>From: {{ $post->name}}</code><br>
                    @endif
                    &nbsp;&nbsp;&nbsp;<code>Date: <em>{{$post->created_at}}</em></code><br>

                    @if($post->source=="Tutorial")
                        <div class=forum_message>{{$post->message}}</div>
                    @else
                        <div class=forum_message>{{$post->message}}</div>
                    @endif


                    @if(strlen($post->url)>0)
                        @if(strtolower(substr($post->url,0,7))=='http://' || strtolower(substr($post->url,0,8))=='https://'||
                        strtolower(substr($post->url,0,6))=='ftp://'|| strtolower(substr($post->url,0,9))=='mailto://' ||
                        strtolower(substr($post->url,0,7))=='news://')
                            $outurl=$post->url;
                        @else
                            $outurl='http://www.google.com/search?q='.$post->url;
                        @endif
                        <h4><a href="{{$outurl}}" rel="nofollow">{{$post->url}}</a></h4>
                    @endif

                    @if(strlen($post->code)>0)
                        <h4>Code:</h4>
                        <code>{{htmlentities($post->code)}}</code>
                    @endif

                    <div class="id">{{$post->id}},{{$post->thread_id}}</div>
                    <div class="button-line">
                        @if(Auth::check()&&((Auth::user()->user_id == $post->user_id)||(Auth::user()->position == 'Staff')))
                            <a class=button href="/news/edit?id={{$post->id}}" rel="nofollow">Edit</a>&nbsp;&nbsp;&nbsp;
                        @endif
                    </div>
                </div>
        </div>

        @endforeach
    </div>
    </div>
@stop

@section('scripts')
@stop

