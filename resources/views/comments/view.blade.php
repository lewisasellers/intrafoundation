@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div align=right>
                <a class=button href="/comments/">List all</a>&nbsp;&nbsp;&nbsp;
                @if(Auth::check())
                    <a class=button href="/comments/add" rel="nofollow">New Post</a>
                @endif
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @foreach($comments as $comment)
                <div class="comment">
                    @if (strlen($comment->product_code) > 0)
                        <code>Comment about: <a
                                    href="/software/{{$comment->product_code}}">{{$comment->product_code}}</a></code>

                        (<i><a href="/comments/product-code/{{$comment->product_code}}">All
                                comments</a></i>)<br>
                    @endif

                    @if ($comment->source == "Comment")
                        <h2><span>{{$comment->source}}</span>
                            <small>View</small>
                            <a href="/software/{{$comment->product_code}}">{{$comment->title}}</a></h2>
                    @elseif ($comment->source == "comments")
                        <h2><span>{{$comment->source}}</span> <a href="/comments/view/{{$id}}">{{$comment->title}}</a>
                        </h2>
                    @else
                        <h2>{{htmlentities($comment->title)}}</h2>
                    @endif

                    @if (strlen($comment->name) > 0)
                        <code>From: {{$comment->name}}</code><br>
                    @endif

                    <code>Date: <em>{{$comment->created_at}}</em></code><br>

                    @if ($comment->source == "Tutorial")
                        <div class=forum_message>{{$comment->message}}</div>
                    @else
                        <div class=forum_message>{{$comment->message}}</div>
                    @endif

                    @if (strlen($comment->url) > 0)
                        <h4><a href="{{$comment->searchUrl}}" rel="nofollow">{{$comment->url}}</a></h4>
                    @endif

                    @if (strlen($comment->code) > 0)
                        <h4>Code:</h4>
                        <code>{{htmlentities($comment->code)}}</code>
                    @endif

                    <div class="id">{{$comment->id}},{{$comment->thread_id}}</div>
                    <div class="button-line">
                        @if(Auth::check()&&((Auth::user()->user_id == $comment->user_id)||(Auth::user()->position == 'Staff')))
                            <a class=button href="/comments/delete?id={{$comment->id}}" rel="nofollow">Delete</a>
                            &nbsp;
                            <a class=button href="/comments/edit?id={{$comment->id}}" rel="nofollow">Edit</a>
                            &nbsp;
                            &nbsp;
                        @endif

                        @if(Auth::check())
                            <a class=button href="/comments/reply?id={{$comment->id}}" rel="nofollow">Reply</a>
                        @endif
                    </div>

                </div> <!-- /comment -->

            @endforeach
        </div>
    </div>
@stop

@section('scripts')
@stop