@extends('layouts.default')

@section('content')
    <div class="col-md-12">

        <h2>All Threads</h2>

        <div align=right>
            <a class=button href="/comments/">List all</a>&nbsp;&nbsp;&nbsp;
            @if(Auth::check())
                <a class=button href="/comments/add" rel="nofollow">New Post</a>
            @endif
        </div>

        <div class=row>
            <div class=col-md-12>
                @foreach($comments as $comment)
                    <div class="comment-line">

                        <span class=comment-link>
            <span class=source-type>{{$comment->source_type}}</span>
            <a href="/comments/view/{{$comment->id}}">{{$comment->title}}</a>

            <code>
                   @if ($comment->threads == 0)
                    (No replies)
                @elseif ($comment->threads == 1)
                    (<b>1</b> reply)
                @else
                    (<b>{{$comment->threads}}</b> replies)
                @endif
            </code>

                        <small>
                            @if (strlen($comment->name) > 0)
                                <nobr><b>{{$comment->name}}</b></nobr>
                                @if($comment->created_at>0)
                                    &nbsp;&nbsp;&nbsp;
                                @endif
                                <nobr class=tt>{{$comment->created_at}}</nobr>
                            @endif
                        </small>

                        </span>
                    </div>
                @endforeach
            </div>
        </div>

    </div>
@stop

@section('scripts')
@stop