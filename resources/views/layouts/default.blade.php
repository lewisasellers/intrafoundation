<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{$meta_title}} - Intrafoundation Software</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="vi wport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{$meta_title}}">
    <meta name="author" content="">

    <link media="all" type="text/css" rel="stylesheet" href="/css/bootstrap.css">
    <link media="all" type="text/css" rel="stylesheet" href="/css/ui.css?ver=12032014">

    <link rel="shortcut icon" href="/ico/favicon.png">
</head>

<body>

<div class="navbar navbar-default navbar-fixed-top" role="navigation">

    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mast-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Intrafoundation Software</a>
    </div>

    <div class="collapse navbar-collapse" id="mast-collapse">
        <ul class="nav navbar-nav">

            <li class="active"><a href="/">Home</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Software<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    @foreach($categories as $category_id=>$category)
                        <li><a href="/software/category/{{$category_id}}">{{$category->title}}</a></li>
                    @endforeach
                </ul>
            </li>
            <li><a href="/comments">Comments</a></li>
            <li><a href="/news">News</a></li>
            <li><a href="http://github.com/lasellers">Github</a></li>
            <li><a href="https://knoxdevs.slack.com/team/lasellers">Chat on Slack</a></li>
            <!--li><a href="http://lewisasellers.com">lewisasellers.com</a></li-->
        </ul>

    </div>


</div>

<div id="blank_nav">
</div>
<!--
<div class="row">
 <form class="navbar-form navbar-right" role="search">
  <div class="form-group">
    <input type="text" class="form-control" placeholder="Search">
  </div>
  <button type="submit" class="btn btn-default">Search</button>
</form>
</div>
-->
<div class="container">
    <?php
    /* Session::reflash();
    */ ?>
    @if(Session::has('info'))
        <div class="alert alert-info">{{ Session::get('info') }}</div>
    @endif

    @if(Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif

    @if(Session::has('error'))
        <div class="alert alert-error">{{ Session::get('error') }}</div>
    @endif

    @if(Session::has('message'))
        <div class="alert">{{ Session::get('message') }}</div>
    @endif

    @yield('content')

    <hr>

    <div class="row" id="status-dialog">
        <img src="/images/spinner.gif">
    </div>

    <nav id="footer" class="navbar navbar-default navbar-fixed-bottom" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#footer-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <small>&copy; Intrafoundation Software 1998-2017</small>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="footer-collapse">
            <ul class="nav navbar-nav">
                <!--  <li class="active"><a href="/profile">Profile</a></li>-->
                <li><a href="/about-us">About Us</a></li>
                <li><a href="/contact-us">Contact Us</a></li>


            </ul>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>

        </div><!-- /.navbar-collapse -->
    </nav>

    @include("partials.login_box")


    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/ui.js?ver=12032013"></script>

    <!--script src="/jquery/ui/js/jquery-1.9.1.js"></script-->
    <!--script src="/jquery/ui/js/jquery-ui-1.10.3.min.js"></script-->
    <!--script src="/jquery/jquery.cookie.js?ver=20140509"></script-->
    <!--script src="/bootstrap/js/bootstrap.min.js"></script-->
@yield('scripts')
</body>
</html>
