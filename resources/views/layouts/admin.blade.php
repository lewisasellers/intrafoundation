<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{$meta_title}} - Intrafoundation Software</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="vi wport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{$meta_title}}">
    <meta name="author" content="">

    <!-- the styles -->
    {{ HTML::style('/css/bootstrap.css') }}
    {{ HTML::style('/css/ui.css?ver=12032013') }}
    {{ HTML::style('/css/api.css?ver=12032013') }}

    <link rel="shortcut icon" href="/ico/favicon.png">
</head>

<body>
<?php
$categories = \App\Products::get_categories();
?>
<div class="navbar navbar-default navbar-fixed-top" role="navigation">

    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mast-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">ADMIN Intrafoundation Software</a>
    </div>

    <div class="collapse navbar-collapse" id="mast-collapse">
        <ul class="nav navbar-nav">

            <li class="active"><a href="/">Home</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Software<b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <?php

                    foreach($categories as $category_id=>$category)
                    {?>
                    <li><a href="/software/category/{{$category_id}}">{{$category->title}}</a></li>
                    <?php }
                    ?>
                </ul>
            </li>
            <li><a href="/comments">Comments</a></li>
            <li><a href="/news">News</a></li>
            <li><a href="/about-us">About Us</a></li>
            <!--<li><a href="/contact-us">Contact Us</a></li>-->
            <li><a href="http://github.com/lasellers">Github</a></li>
        </ul>

    </div>

</div>

<div id="blank_nav">
</div>
<!--
<div class="row">
 <form class="navbar-form navbar-right" role="search">
  <div class="form-group">
    <input type="text" class="form-control" placeholder="Search">
  </div>
  <button type="submit" class="btn btn-default">Search</button>
</form>
</div>
-->
<div class="container">
    <?php
    /* Session::reflash();
    */ ?>
    @if(Session::has('info'))
        <div class="alert alert-info">{{ Session::get('info') }}</div>
    @endif

    @if(Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif

    @if(Session::has('error'))
        <div class="alert alert-error">{{ Session::get('error') }}</div>
    @endif

    @if(Session::has('message'))
        <div class="alert">{{ Session::get('message') }}</div>
    @endif

    @yield('content')

    <hr>

    <div class="row" id="status-dialog">
        <img src="/images/spinner.gif">
    </div>

    <nav id="footer" class="navbar navbar-default navbar-fixed-bottom" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#footer-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <small>&copy; Intrafoundation Software 1998-2014</small>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="footer-collapse">
            <ul class="nav navbar-nav">
                <!--  <li class="active"><a href="/profile">Profile</a></li>-->
                <li><a href="/contact-us">Contact Us</a></li>
                <!--  <li><a href="/search">Search</a></li>-->
            <!--
    <?php if(!Auth::check()) { ?>
                    <li><a class="login" href="/login">Login</a></li>
                    <li><a>or</a></li>
                    <li><a class="signup" href="/signup">Signup</a></li>
                    <?php } else { ?>
                    <li><a class="logout" href="/logout">Logout from <?php echo Auth::user()->public_name; ?>&nbsp;</a></li>
    <?php } ?>
                    <li> </li>
                  -->
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>

    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/ui.js?ver=12032013"></script>
    <script src="/js/api.js?ver=12032013"></script>
@yield('scripts')
</body>
</html>
