<?php
header("Content-Type: text/xml;charset=iso-8859-1");
echo '<'.'?'.'xml version="1.0" encoding="UTF-8"'.'?'.'>'."\r\n";
$absurl='http://www.intrafoundation.com/';
?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<url><loc><?php echo $absurl?></loc></url>
<url><loc><?php echo $absurl?>about-us/</loc></url>
<url><loc><?php echo $absurl?>contact-us/</loc></url>
<url><loc><?php echo $absurl?>software/</loc></url>
<?php
  foreach($categories as $category=>$title)
  {
?>
<url><loc><?php echo $absurl?>software/category/<?php echo $category?></loc></url>
<?php
  }
?>
<url><loc><?php echo $absurl?>software/</loc></url>
<?php
  foreach($products as $product)
  {
?>
<url><loc><?php echo $absurl?>software/<?php echo $product->product_code?></loc></url>
<?php
  }
?>
<url><loc><?php echo $absurl?>news/</loc></url>
<url><loc><?php echo $absurl?>comments/</loc></url>
<?php
   foreach($comments as $comment)
  {
?>
<url><loc><?php echo $absurl?>comment/view/<?php echo $comment->id?></loc></url>
<?php
  }
?>
</urlset>
