@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-md-6 panel-main">

            <div class="row">
                <div class="col-md-12 introduction" style="display: none;">
                    <img src="{{$CDN}}/images/home1.jpg" class="top-right">
                    <p>Since 1998 we&#39;ve been doing contract software development primarily focused on backend web
                        tools (ASP/ColdFusion COM objects, ColdFusion C/C++ and Java CFX extensions, etc.)
                        We generally focus on tool-building for engineers and web design firms.
                        We&#39;ve also worked on several websites themselves in a variety of languages.
                    </p>
                    <p>
                        Here, at this site, you&#39;ll find a growing collection of useful (and mostly free,
                        open-sourced) bits that are the fallout of previous work we&#39;ve done.
                    </p>
                    <p>
                        We also may, from time to time, sell a few items that we&#39;ve labored on recently.
                        If need help with anything here, just ask. Or if you need "a custom part machined" for your
                        website, we&#39;re of course always available for such work.
                    </p>
                    <div class="button-line">
                        <small><a href="/news/">complete news</a></small>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    @foreach($comments as $comment)
                        <div class="comment-short">
                            <b><a href="comments/view/{{$comment->id}}">{{$comment->title}}</a></b><br>
                            <small><code>{{$comment->created_at}}</code></small>
                            <br>
                            @if (strlen($comment->message) > 132)
                                <div class=description>{{$comment->read_me}} ...
                                    <small><a href="comments/view/{{$comment->id}}">Read More</a></small>
                                </div><br>
                            @else
                                <div class=description>{{$comment->message}}</div><br>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>

        </div>

        <div class="col-md-6 panel-aside">

            <div class="row">
                <div class="col-md-12" id="downloads">
                    <h2 align=left>Most Downloaded</h2>
                    @foreach ($downloads as $product)
                        <div class="product-short">
                            <h3>
                                <a href="/software/{{$product->product_code}}">{{$product->name}}</a>
                                ({{$product->version}}) <span
                                        class=tt>{{$product->build_date}}</span>
                            </h3>
                            @if(file_exists(public_path()."/content/product_images/{$product->product_id}.jpg"))
                                <div>
                                    <a href="/software/{{$product->product_code}}">
                                        <img src="/content/product_images/{{$product->product_id}}.jpg" class="product">
                                    </a>
                                </div>
                            @endif
                            <small>(<em>{{$product->downloads}} downloads</em>)</small>

                            <div class=info>
                                <small>{{$product->platform}}&nbsp;
                                    @if($product->technology!='')
                                        <b>{{$product->technology}}</b>
                                    @endif
                                </small>
                                @if($product->source_code)
                                    [w/ {{$product->source_language}} source code]
                                @endif
                            </div>

                            <p>{{($product->blurb)}}</p>
                            <div class=button-line>
                                <a class=button href="/software/{{$product->product_code}}">view</a>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>


            <div class="row">
                <div class="col-md-12" id="products">
                    <h2 align=left>Recent Updates</h2>
                    @foreach ($products as $product)
                        <div class="product-short" style="display: block">
                            <h3>
                                <a href="/software/{{$product->product_code}}">{{$product->name}}</a>
                                ({{$product->version}}) <span
                                        class=tt>{{$product->build_date}}</span>
                                @if(file_exists(public_path()."/content/product_images/{$product->product_id}.jpg"))
                                    <div>
                                        <a href="/software/{{$product->product_code}}">
                                            <img src="/content/product_images/{{$product->product_id}}.jpg"
                                                 class="product">
                                        </a>
                                    </div>
                                @endif
                                <small>(<em>{{$product->downloads}} downloads</em>)</small>
                            </h3>
                            <div class=info>
                                <small>{{$product->platform}}&nbsp;
                                    @if($product->technology!='')
                                        <b>{{$product->technology}}</b>
                                    @endif
                                </small>
                                @if($product->source_code)
                                    [w/ {{$product->source_language}} source code]
                                @endif
                            </div>
                            <p>{{($product->blurb)}}</p>
                            <div class=button-line>
                                <a class=button href="/software/{{$product->product_code}}">view</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(document).ready(function () {
            setTimeout(showLogo, 1000);
            function showLogo() {
                console.log("show logo");
                //$(".slider").show("slow");
                //$(".introduction").show("slow");

                $(".introduction").animate({
                    /*  opacity: 0.25,*/
                    top: "0",
                    height: "toggle"
                }, 5000, function () {
                    // Animation complete.
                });
            };
        });

        /*
         $(document).ready(function()
         {
         //
         var len=$("#products div").length;
         console.log("len="+len);
         if(len>1)
         {
         var i=Math.floor((Math.random()*len));
         $("#products div").hide();
         $("#products div:eq("+i+")").fadeIn("slow");
         }

         //
         $("#products").find("div:gt(0)").hide();
         setTimeout(products_autoplay, 5000);

         function products_autoplay() {
         var current = $("#products").find("div:visible");

         var next = current.next();
         if (next.length == 0) next = $("#products").find("div:eq(0)");
         current.hide();
         next.show();

         setTimeout(products_autoplay, 5000);
         }

         $(".featured-click").click(function(e) {
         e.preventDefault();
         var current = $("#featured").find("li:visible");
         current.hide();
         var i=$(this).text();
         $("#featured").find("li:eq("+(i-1)+")").show();
         });

         $("#products").swipe( {
         swipe:function(event, direction, distance, duration, fingerCount) {
         if(direction=='right')
         {
         var current = $("#featured").find("li:visible");
         var next = current.previous();
         if (next.length != 0) {
         current.hide();
         next.show();
         }
         }
         else if(direction=='left')
         {
         var current = $("#featured").find("li:visible");
         var next = current.next();
         if (next.length == 0) {}
         current.hide();
         next.show();
         }
         },
         threshold:0
         });
         });
         */
    </script>
@stop
