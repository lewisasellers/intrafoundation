@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <h1>{{$meta_title}}</h1>

        <p>
            <strong>
                Intrafoundation Software</strong> has been involved in software development for web-related projects
            (generally backend or CMS) as well as the development of websites themselves since 1997.
            From 2006 to 2012 we did more traditional web design work under the auspices of <a
                    href="http://www.cedargrovedesign.com">Cedar Grove Design</a>.
        </p>
        <p class="para">
            Trivia: The site/domain was originally called "Fourth Foundation". &nbsp;The name "Intrafoundation" was
            chosen in 1998 because it was a loose literal interpretation of software we were working with then and
            because the word did not otherwise exist anywhere on the internet at the time [except for one expired legal
            reference in Canada].</p>

        <p>
            Additionally, if you were curious: <strong>
                <em>&#39;Making Atomic Warfare Fun Again&#39;</em> has been our long running slogan (or gag-line) since
                very near the beginning</strong>.
            We&#39;re pro-Atomic explosions. Pro-Military.
            And pro-Zombie hordes.
            Um. Ok, ignore that last part.</p>
        <p>
            A little more seriously, we have sold software at times to the <strong>US Navy, NASA&#39;s Prime Contractor
                USA</strong>, and various other MILSPEC folk who escape me at the moment.

        </p>
        <img src="{{$CDN}}/images/makingatomicwarfarefunagain.smlogo.jpg" alt="Min in pseudo-military dress"
             align="right" border="0" class="image">

        Lewis A. Sellers (AKA Min)
        <p>
            Founder of Intrafoundation Software.
        </p>
        <ul>
            <li><a href="http://lewisasellers.com">My Blog, Eclectic Observations</a>.<br>
            </li>
            <li>
                <a href="http://www.cedargrovedesign.com">Need a new web site, brother?</a> (<em>cedar grove design</em>)<br>
            </li>
            <li><a class="url" href="http://www.happyplacegames.com">Casual Game Development</a>
            </li>
            <li><a class="url" href="http://www.communalperfection.com">Communal Perfection (Programming)</a>
            </li>
        </ul>
        <br>
        <ul>
            <li><a href="https://www.facebook.com/pages/Intrafoundation-Software/366689930079215">Intrafoundation
                    Software Facebook Page</a>
            </li>
            <li><a href="https://www.facebook.com/CommunalPerfection">Communal Perfection Facebook Page</a>
            </li>
        </ul>

        <br>


        <p>
            In the past we were host to two amateur OS startups as well.
            Those sites are still archived here.
        </p>
        <ul>
            <li><a href="http://eos.intrafoundation.com">EOS (Entertainment Operating System)</a>
            </li>
            <li><a href="http://foundation.intrafoundation.com">Foundation (Grail Millennium Foundation)</a>
            </li>
        </ul>


    </div>
@stop

@section('scripts')
@stop
