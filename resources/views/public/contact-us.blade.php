@extends('layouts.default')

@section('content')
    <div class="col-md-12">
        <h1>{{$meta_title}}</h1>

        {{ Form::open(array('url' => 'contact-us')) }}

        <p>
            {{ $errors->first('email') }}
            {{ $errors->first('password') }}
            {{ $errors->first('comment') }}
        </p>

        <p>
            {{ Form::label('name', 'Name') }}<br>
            {{ Form::text('name', Request::old('name'), array('placeholder' => 'Your name')) }}
        </p>
        <p>
            {{ Form::label('email', 'Email Address') }}<br>
            {{ Form::text('email', Request::old('email'), array('placeholder' => 'your@mail.com')) }}
        </p>

        <p>
            {{ Form::label('comment', 'Comment') }}<br>
            {{ Form::textarea('comment', Request::old('comment'), array('placeholder' => 'Your comment')) }}
        </p>

        <p>{{ Form::submit('Contact Us') }}</p>
        {{ Form::close() }}

    </div>
@stop

@section('scripts')
@stop
